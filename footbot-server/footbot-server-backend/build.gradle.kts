description = "Footbot server backend"

group = "ru.kovpas.footbot.server.backend"
version = "0.1.0"

repositories {
    maven { url = uri("https://dl.bintray.com/kotlin/ktor") }
}

plugins {
    application
    kotlin("jvm")
}

application {
    mainClassName = "io.ktor.server.netty.DevelopmentEngine"
}

val kotlin_version: String by project
val ktor_version: String by project
val junit_version: String by project
val logback_classic_version: String by project
val aza_kotlin_css_version: String by project

dependencies {
    compile(kotlin("stdlib-jdk8"))
    compile("io.ktor", "ktor-locations", ktor_version)
    compile("io.ktor", "ktor-server-netty", ktor_version)
    compile("io.ktor", "ktor-html-builder", ktor_version)
    compile("ch.qos.logback", "logback-classic", logback_classic_version)
    compile("azadev.kotlin", "aza-kotlin-css", aza_kotlin_css_version)

    compile(project(":footbot-core:footbot-core-jvm-model"))
    compile(project(":footbot-scraper:footbot-scraper-sportsru"))

    testCompile("org.junit.jupiter", "junit-jupiter-api", junit_version)
    testCompile("org.junit.jupiter", "junit-jupiter-params", junit_version)
    testRuntime("org.junit.jupiter", "junit-jupiter-engine", junit_version)
    testCompile("org.jetbrains.kotlin", "kotlin-test-junit5", kotlin_version)
}

package ru.kovpas.footbot.server.backend.api.scrape

import io.ktor.application.call
import io.ktor.http.ContentType
import io.ktor.locations.get
import io.ktor.response.respondText
import io.ktor.routing.Route

fun Route.scrapeRoot() {
    get<Index> {
        call.respondText("Scrape.Root", ContentType.Text.Html)
    }
}

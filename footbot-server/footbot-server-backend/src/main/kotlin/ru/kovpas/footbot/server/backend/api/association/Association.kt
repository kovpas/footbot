package ru.kovpas.footbot.server.backend.api.association

import io.ktor.routing.Route
import io.ktor.routing.route

const val associationRootPath = "association"

fun Route.association() {
    route(associationRootPath) {
        associationRoot()
        associationLeagues()
    }
}

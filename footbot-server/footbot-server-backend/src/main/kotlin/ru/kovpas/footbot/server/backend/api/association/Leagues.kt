package ru.kovpas.footbot.server.backend.api.association

import io.ktor.application.call
import io.ktor.http.ContentType
import io.ktor.locations.get
import io.ktor.response.respondText
import io.ktor.routing.Route
import ru.kovpas.footbot.core.model.Association

fun Route.associationLeagues() {
    get<Leagues> {
        val association = Association.findByAbbreviation(it.abbreviation)
        if (association != null) {
            val responseString = association.leagues().joinToString(",\n") { league -> league.json() }
            call.respondText("[\n$responseString\n]", ContentType.Text.Plain)
        } else {
            call.respondText("Association ${it.abbreviation} not found", ContentType.Text.Plain)
        }
    }
}

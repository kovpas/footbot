package ru.kovpas.footbot.server.backend.api.scrape

import io.ktor.application.call
import io.ktor.http.ContentType
import io.ktor.locations.get
import io.ktor.response.respondText
import io.ktor.routing.Route
import ru.kovpas.footbot.core.model.Association

fun Route.scrapeLeagues() {
    get<Leagues> {
        val association = Association.findByAbbreviation("rus")!!
        val leagues = sportsruScraper.leagues(association)
        call.respondText("${leagues.map { league -> "\"${league.title}\"" }}", ContentType.Text.Html)
    }
}
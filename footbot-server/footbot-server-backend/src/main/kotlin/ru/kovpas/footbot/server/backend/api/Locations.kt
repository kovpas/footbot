package ru.kovpas.footbot.server.backend.api

import io.ktor.locations.Location

@Location("/")
class Index

@Location("/initialize")
class Initialize(val type: String? = null, val force: Boolean = false)

@Location("/associations")
class Associations

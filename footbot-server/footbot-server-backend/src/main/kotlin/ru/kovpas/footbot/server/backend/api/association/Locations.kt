package ru.kovpas.footbot.server.backend.api.association

import io.ktor.locations.Location

@Location("/{abbreviation}")
class Index(val abbreviation: String)

@Location("/{abbreviation}/leagues")
class Leagues(val abbreviation: String)


package ru.kovpas.footbot.server.backend

import io.ktor.application.Application
import io.ktor.application.call
import io.ktor.application.install
import io.ktor.application.log
import io.ktor.features.CallLogging
import io.ktor.features.DefaultHeaders
import io.ktor.features.StatusPages
import io.ktor.http.HttpStatusCode
import io.ktor.locations.Locations
import io.ktor.response.respond
import io.ktor.routing.Routing
import io.ktor.util.KtorExperimentalAPI
import ru.kovpas.footbot.server.backend.api.api
import ru.kovpas.footbot.server.backend.frontend.frontend

@KtorExperimentalAPI
@Suppress("unused") // Referenced in application.conf
fun Application.main() {
    install(DefaultHeaders)

    val config = environment.config
    val serviceConfig = config.config("service")
    val ktorConfig = config.config("ktor")
    val mode = serviceConfig.property("environment").getString()
    log.info("Environment: $mode")
    val port = ktorConfig.config("deployment").property("port").getString()
    log.info("Port: $port")
    val production = mode == "production"

    install(Locations)

    install(StatusPages) {
        exception<NotImplementedError> { call.respond(HttpStatusCode.NotImplemented) }
    }

    if (!production) {
        install(CallLogging)
    }
    
    install(Routing) {
        frontend()
        api()
    }
}

package ru.kovpas.footbot.server.backend.api.scrape

import io.ktor.locations.Location

@Location("/")
class Index

@Location("/leagues")
class Leagues

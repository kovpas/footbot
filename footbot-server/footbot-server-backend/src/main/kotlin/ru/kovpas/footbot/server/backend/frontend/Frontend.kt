package ru.kovpas.footbot.server.backend.frontend

import io.ktor.application.call
import io.ktor.html.respondHtml
import io.ktor.locations.get
import io.ktor.routing.Route
import io.ktor.routing.route
import kotlinx.html.*

const val frontendRootPath = ""

fun Route.frontend() {
    route(frontendRootPath) {
        frontendRoot()
    }
}

fun Route.frontendRoot() {
    get<Index> {
        call.respondHtml {
            head {
                title { +"Footbot" }
                style { +CSS.index() }

                link(rel = "stylesheet", href = "https://fonts.googleapis.com/icon?family=Material+Icons")
                link(rel = "stylesheet", href = "https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css")
                script(src = "https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js") { }

                script(src = "frontend/footbot-server-frontend.bundle.js") { }
            }
            body {
                div(classes = "container") { id = "footbot-root" }
            }
        }
    }
}

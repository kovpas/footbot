package ru.kovpas.footbot.server.backend.api.scrape

import io.ktor.routing.Route
import io.ktor.routing.route
import ru.kovpas.footbot.scraper.HtmlScraper
import ru.kovpas.footbot.scraper.HttpClient
import ru.kovpas.footbot.scraper.SportsruScraper

const val scrapeRootPath = "scrape"
val sportsruScraper = SportsruScraper(htmlScraper = HtmlScraper(client = HttpClient()))

fun Route.scrape() {
    route(scrapeRootPath) {
        scrapeRoot()
        scrapeLeagues()
    }
}

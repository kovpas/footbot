package ru.kovpas.footbot.server.backend.api

import io.ebean.Ebean
import io.ktor.application.call
import io.ktor.http.ContentType
import io.ktor.locations.delete
import io.ktor.locations.get
import io.ktor.response.respondText
import io.ktor.routing.Route
import ru.kovpas.footbot.core.model.Association

fun Route.apiAssociations() {
    get<Associations> {
        val associations = Association.all()
        val responseString = associations.joinToString(",\n") { association -> association.json() }
        call.respondText("[\n$responseString\n]", ContentType.Text.Plain)
    }

    delete<Associations> {
        Ebean.deleteAll(Association.all())
        call.respondText("Deleted all associations", ContentType.Text.Plain)
    }
}

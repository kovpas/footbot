package ru.kovpas.footbot.server.backend.api

import io.ktor.routing.Route
import io.ktor.routing.route
import ru.kovpas.footbot.server.backend.api.association.association
import ru.kovpas.footbot.server.backend.api.scrape.scrape

const val apiRootPath = "api"

fun Route.api() {
    route(apiRootPath) {
        apiRoot()

        apiInitialize()
        apiAssociations()

        association()
        scrape()
    }
}

package ru.kovpas.footbot.server.backend.api

import io.ktor.application.call
import io.ktor.locations.get
import io.ktor.response.respondRedirect
import io.ktor.routing.Route

fun Route.apiRoot() {
    get<Index> {
        call.respondRedirect("/$apiRootPath/associations")
    }
}

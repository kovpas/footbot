package ru.kovpas.footbot.server.backend.api

import io.ktor.application.ApplicationCall
import io.ktor.application.application
import io.ktor.application.call
import io.ktor.application.log
import io.ktor.http.ContentType
import io.ktor.locations.get
import io.ktor.response.respondText
import io.ktor.routing.Route
import io.ktor.util.pipeline.PipelineContext
import org.yaml.snakeyaml.Yaml
import ru.kovpas.footbot.core.model.Association
import ru.kovpas.footbot.core.model.AssociationType
import ru.kovpas.footbot.core.model.Continent
import ru.kovpas.footbot.core.model.Country
import java.io.File


fun Route.apiInitialize() {
    get<Initialize> { initialize ->
        val force = initialize.force
        val type = initialize.type

        if (type == null || type == "continents") {
            initializeContinents(force)
        }
        if (type == null || type == "countries") {
            initializeCountries(force)
        }
        if (type == null || type == "associations") {
            initializeAssociations(force)
        }
        call.respondText("Initialized", ContentType.Text.Html)
    }
}

val yaml = Yaml()

fun PipelineContext<Unit, ApplicationCall>.initializeContinents(force: Boolean = false) {
    if (force) {
        application.log.info("Re-initializing continents. Removing old ones...")
        Continent.deleteAll()
    }

    if (Continent.all().count() != 0) {
        application.log.warn("Attempt to initialize non-empty continents. Set `force` to `true` to reinitialize.")
        return
    }

    application.log.info("Initializing continents")
    val fileContent = ClassLoader.getSystemResource("data/world/continents.yml")
    val continentList = yaml.load<List<LinkedHashMap<String, *>>>(fileContent.readText())

    continentList.forEach { continentYaml ->
        val continent = Continent(
            code = continentYaml["code"] as String,
            name = continentYaml["name"] as String
        )
        continent.save()
    }

    application.log.info("Continents initialized")
}

fun PipelineContext<Unit, ApplicationCall>.initializeCountries(force: Boolean = false) {
    if (force) {
        application.log.info("Re-initializing countries. Removing old ones...")
        Country.deleteAll()
    }

    if (Country.all().count() != 0) {
        application.log.warn("Attempt to initialize non-empty countries. Set `force` to `true` to reinitialize.")
        return
    }

    application.log.info("Initializing countries")
    val files = getResourceFolderFiles("data/world/continents")
    files.forEach {
        application.log.debug("Initializing countries from ${it.name}")
        val countriesList = yaml.load<List<LinkedHashMap<String, *>>>(it.readText())

        countriesList.forEach { countryYaml ->
            application.log.debug("Initializing country $countryYaml")
            val continent = Continent.findByCode(countryYaml["continentCode"] as String)!!
            val country = Country(
                code = countryYaml["code"] as String,
                code3 = countryYaml["code3"] as String,
                name = countryYaml["name"] as String,
                continent = continent
            )
            country.save()
        }
    }

    application.log.info("Countries initialized")
}

fun PipelineContext<Unit, ApplicationCall>.initializeAssociations(force: Boolean = false) {
    if (force) {
        application.log.info("Re-initializing associations. Removing old ones...")
        Association.deleteAll()
    }

    if (Association.all().count() != 0) {
        application.log.warn("Attempt to initialize non-empty associations. Set `force` to `true` to reinitialize.")
        return
    }

    application.log.info("Initializing associations")
    val fileContent = ClassLoader.getSystemResource("data/associations.yml")
    val associationsList = yaml.load<List<LinkedHashMap<String, *>>>(fileContent.readText())

    associationsList.forEach { associationYaml ->
        application.log.debug("Initializing association $associationYaml")

        val parentAssociations = if (associationYaml["parentAssociations"] != null) {
            val parentAssociationsYaml = (associationYaml["parentAssociations"] as List<*>).filterIsInstance<String>()
            Association.findByAbbreviations(parentAssociationsYaml)
        } else {
            listOf()
        }

        val countryCode = associationYaml["countryCode"] as String?
        val country: Country? = if (countryCode != null) {
            Country.findByCode(countryCode)
        } else {
            null
        }

        val association = Association(
            abbreviation = associationYaml["abbreviation"] as String,
            name = associationYaml["name"] as String,
            type = AssociationType.valueOf(associationYaml["type"] as String),
            parentAssociations = parentAssociations,
            country = country
        )
        association.save()
    }
    application.log.info("Associations initialized")
}

private fun getResourceFolderFiles(folder: String): Array<File> {
    val loader = Thread.currentThread().contextClassLoader
    val url = loader.getResource(folder)
    val path = url.path
    return File(path).listFiles()
}

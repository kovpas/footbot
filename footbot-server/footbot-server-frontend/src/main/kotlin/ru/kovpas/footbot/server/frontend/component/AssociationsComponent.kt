package ru.kovpas.footbot.server.frontend.component

import kotlinx.coroutines.Dispatchers
import react.RBuilder
import react.RProps
import react.ReactElement
import react.setState
import ru.kovpas.footbot.core.model.Association
import ru.kovpas.footbot.server.frontend.BaseComponent
import ru.kovpas.footbot.server.frontend.BaseState
import ru.kovpas.footbot.server.frontend.presenter.associations.AssociationsPresenter
import ru.kovpas.footbot.server.frontend.presenter.associations.AssociationsView
import ru.kovpas.footbot.server.frontend.repository.AssociationsRepository
import ru.kovpas.footbot.server.frontend.view.*
import kotlin.properties.Delegates.observable

class AssociationsComponent : BaseComponent<RProps, AssociationsComponentState>(), AssociationsView {

    private val associationsRepository = AssociationsRepository()

    @Suppress("Unused")
    private val presenter by presenter { AssociationsPresenter(Dispatchers.Default, this, associationsRepository) }

    override var loading: Boolean by observable(false) { _, _, n ->
        setState { loading = n }
    }
    override var refresh: Boolean by observable(false) { _, _, n ->
        setState { loading = n }
    }

    override fun RBuilder.render() {
        when {
            loading -> loadingView()
            state.error != null -> errorView(state.error!!)
            else -> associationsListView()
        }
    }

    private fun RBuilder.associationsListView(): ReactElement? {
        headerView(listOf(HeaderItem("associations", "associations")))
        return associationsListView(state.associations.orEmpty(), presenter::filterAssociations)
//        fabView()
    }

    override fun showList(associations: List<Association>) {
        setState { this.associations = associations }
    }

}

external interface AssociationsComponentState : BaseState {
    var loading: Boolean?
    var associations: List<Association>?
}

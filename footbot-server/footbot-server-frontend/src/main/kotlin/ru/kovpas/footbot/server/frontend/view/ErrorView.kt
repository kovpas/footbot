package ru.kovpas.footbot.server.frontend.view

import react.RBuilder
import react.ReactElement
import react.dom.div
import react.dom.h3
import ru.kovpas.footbot.server.frontend.HttpError

fun RBuilder.errorView(error: Throwable): ReactElement? = div(classes = "error") {
    val message = if (error is HttpError) "Http ${error.code} error :(<br>Message: ${error.message}" else error.message.orEmpty()
    h3(classes = "center-on-screen") { +message }
}
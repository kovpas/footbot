package ru.kovpas.footbot.server.frontend

class HttpError(val code: Int, override val message: String) : Throwable()

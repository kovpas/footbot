package ru.kovpas.footbot.server.frontend.component

import kotlinx.coroutines.Dispatchers
import react.RBuilder
import react.RProps
import react.ReactElement
import react.setState
import ru.kovpas.footbot.core.model.Association
import ru.kovpas.footbot.server.frontend.AssociationProps
import ru.kovpas.footbot.server.frontend.BaseComponent
import ru.kovpas.footbot.server.frontend.BaseState
import ru.kovpas.footbot.server.frontend.presenter.association.AssociationPresenter
import ru.kovpas.footbot.server.frontend.presenter.association.AssociationView
import ru.kovpas.footbot.server.frontend.repository.AssociationRepository
import ru.kovpas.footbot.server.frontend.view.*
import kotlin.properties.Delegates

class AssociationComponent : BaseComponent<AssociationProps, AssociationComponentState>(),
    AssociationView {

    private val associationRepository = AssociationRepository()

    @Suppress("Unused")
    private val presenter by presenter { AssociationPresenter(Dispatchers.Default, this, associationRepository,
        props.abbreviation) }

    override var loading: Boolean by Delegates.observable(false) { _, _, n ->
        setState { loading = n }
    }
    override var refresh: Boolean by Delegates.observable(false) { _, _, n ->
        setState { loading = n }
    }

    override fun showAssociation(association: Association) {
        setState { this.association = association }
    }

    override fun RBuilder.render() {
        when {
            loading -> loadingView()
            state.error != null -> errorView(state.error!!)
            else -> associationView()
        }
    }

    private fun RBuilder.associationView(): ReactElement? {
        val association = state.association ?: return loadingView()
        headerView(listOf(
            HeaderItem("associations", "associations"),
            HeaderItem(association.abbreviation, association.url)))
        return associationView(association, presenter::scrapeLeagues)
//        fabView()
    }
}

external interface AssociationComponentState : BaseState {
    var loading: Boolean?
    var association: Association?
}
package ru.kovpas.footbot.server.frontend

interface BaseView {
    fun logError(error: Throwable)
    fun showError(error: Throwable)
}
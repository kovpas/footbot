package ru.kovpas.footbot.server.frontend

import react.RBuilder
import react.RProps
import react.dom.render
import react.router.dom.*
import ru.kovpas.footbot.server.frontend.component.AssociationComponent
import ru.kovpas.footbot.server.frontend.component.AssociationsComponent
import kotlin.browser.document
import kotlin.browser.window

//external fun require(module: String): dynamic

fun main(args: Array<String>) {
    window.onload = {
        val root = document.getElementById("footbot-root")

        render(root) {
            hashRouter {
                switch {
                    redirect("/", "/associations", exact = true)
                    route("/associations", AssociationsComponent::class)
                    route("/association/:abbreviation", render = this::associationRouteHandler)
                }
            }
        }
    }
}

fun RBuilder.associationRouteHandler(props: RouteResultProps<AssociationProps>) =
    child(AssociationComponent::class) {
        attrs.abbreviation = props.match.params.abbreviation
    }

interface AssociationProps : RProps {
    var abbreviation: String
}
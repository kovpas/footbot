package ru.kovpas.footbot.server.frontend.repository

import kotlinx.coroutines.coroutineScope
import ru.kovpas.footbot.core.model.Association
import ru.kovpas.footbot.core.model.AssociationType
import ru.kovpas.footbot.core.model.Country
import ru.kovpas.footbot.server.frontend.utils.get

class AssociationsRepository {
    private var cachedAssociations: List<Association>? = null

    suspend fun getAssociations(): List<Association> = coroutineScope {
        val result = cachedAssociations
        if (result != null) {
            result
        } else {
            val associationsData = JSON.parse<Array<AssociationData>>(get("api/associations"))
            val associations = transformAssociationsDataToAssociations(associationsData)
            cachedAssociations = associations
            associations
        }
    }

    private fun transformAssociationsDataToAssociations(associationsData: Array<AssociationData>): List<Association> {
        var result: List<Association> = listOf()

        for (ad in associationsData) {
            val parentAssociations: List<Association> =
                ad.parentAssociationAbbreviations.mapNotNull { parentAbbreviation ->
                    result.find { it.abbreviation == parentAbbreviation }
                }
            result += Association(
                id = ad.id,
                abbreviation = ad.abbreviation,
                name = ad.name,
                type = AssociationType.valueOf(ad.type),
                parentAssociations = parentAssociations,
                country = ad.country
            )
        }

        for (association in result) {
            association.childAssociations.addAll(result.filter { it.parentAssociations.contains(association) })
        }

        return result
    }
}

data class AssociationData(
    val id: Long,
    val abbreviation: String,
    val name: String,
    val type: String,
    val country: Country,
    val parentAssociationAbbreviations: Array<String>
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || this::class.js != other::class.js) return false

        other as AssociationData

        if (abbreviation != other.abbreviation) return false
        if (name != other.name) return false
        if (type != other.type) return false
        if (!parentAssociationAbbreviations.contentEquals(other.parentAssociationAbbreviations)) return false

        return true
    }

    override fun hashCode(): Int {
        var result = abbreviation.hashCode()
        result = 31 * result + name.hashCode()
        result = 31 * result + type.hashCode()
        result = 31 * result + parentAssociationAbbreviations.contentHashCode()
        return result
    }

}

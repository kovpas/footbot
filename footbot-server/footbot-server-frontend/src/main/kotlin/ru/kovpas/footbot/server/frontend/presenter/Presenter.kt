package ru.kovpas.footbot.server.frontend.presenter

interface Presenter {
    fun onCreate() {}
    fun onDestroy() {}
}
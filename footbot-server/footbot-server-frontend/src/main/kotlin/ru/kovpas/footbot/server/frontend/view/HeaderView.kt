package ru.kovpas.footbot.server.frontend.view

import react.RBuilder
import react.ReactElement
import react.dom.a
import react.dom.div
import react.dom.nav
import react.dom.span

data class HeaderItem(val title: String, val dest: String)

fun RBuilder.headerView(elements: List<HeaderItem>): ReactElement? = div(classes = "row") {
    nav {
        div(classes = "nav-wrapper") {
            div(classes = "col s12") {
                for (item in elements) {
                    a(href = "#/${item.dest}", classes = "breadcrumb") {
                        +item.title.toLowerCase()
                    }
                }
            }
        }
    }
}
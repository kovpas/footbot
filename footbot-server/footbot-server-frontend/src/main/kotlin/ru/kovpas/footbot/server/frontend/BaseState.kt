package ru.kovpas.footbot.server.frontend

external interface  BaseState: react.RState {
    var error: Throwable?
}
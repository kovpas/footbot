package ru.kovpas.footbot.server.frontend.presenter.associations

import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.w3c.dom.get
import react.dom.findDOMNode
import ru.kovpas.footbot.core.model.Association
import ru.kovpas.footbot.server.frontend.presenter.BasePresenter
import ru.kovpas.footbot.server.frontend.repository.AssociationsRepository
import kotlin.coroutines.CoroutineContext

class AssociationsPresenter(
    private val uiContext: CoroutineContext,
    private val view: AssociationsView,
    private val associationsRepository: AssociationsRepository
) : BasePresenter() {

    private var visibleAssociations: List<Association>? = null

    override fun onCreate() {
        view.loading = true
        refreshList()
    }

    private fun refreshList() {
        jobs += GlobalScope.launch(uiContext) {
            try {
                val associations = associationsRepository.getAssociations()
                if (associations == visibleAssociations) return@launch
                visibleAssociations = associations

                view.showList(associations)
            } catch (e: Throwable) {
                view.showError(e)
            } finally {
                view.refresh = false
                view.loading = false
            }
        }
    }

    fun filterAssociations(filterTerm: String) {
        jobs += GlobalScope.launch(uiContext) {
            val filteredAssociations = associationsRepository.getAssociations().filter {
                it.name.contains(filterTerm, ignoreCase = true) || it.abbreviation.contains(filterTerm, ignoreCase = true)
            }
            view.showList(filteredAssociations)
        }
    }
}
package ru.kovpas.footbot.server.frontend.view

import libraries.react.material.Button
import react.RBuilder
import react.ReactElement
import react.dom.*
import ru.kovpas.footbot.core.model.Association

fun RBuilder.associationView(association: Association, onScrapePress: () -> Unit): ReactElement? =
    div(classes = "row") {
        div(classes = "col s6") {
            div(classes = "card blue-grey darken-1") {
                div(classes = "card-content white-text") {
                    span(classes = "card-title") {
                        +association.name
                    }
                    p {
                        +association.abbreviation
                    }
                }
                div(classes = "card-action") {
                    a(href = association.url) {
                        +"Reload"
                    }
                }
            }
        }
        div(classes = "col s6") {
            div(classes = "card blue-grey darken-1") {
                div(classes = "card-content white-text") {
                    span(classes = "card-title") {
                        +"leagues"
                    }
                    console.log(association)
                    if (association.leagues.count() > 0) {
                        ul(classes = "collection") {
                            association.leagues.forEach { league ->
                                li(classes = "collection-item grey darken-3") {
                                    +league.title
                                }
                            }
                        }
                    }
                }
                div(classes = "card-action") {
                    child(Button::class) {
                        attrs.onClick = {
                            onScrapePress()
                        }
                        +"Scrape"
                    }
                }
            }
        }
    }
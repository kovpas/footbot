package ru.kovpas.footbot.server.frontend.view

import react.RBuilder
import react.ReactElement
import react.dom.div

fun RBuilder.loadingView(): ReactElement? =
    div(classes = "progress") {
        div(classes = "indeterminate") {}
    }
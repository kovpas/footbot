package ru.kovpas.footbot.server.frontend.presenter.associations

import org.w3c.dom.Element
import ru.kovpas.footbot.core.model.Association
import ru.kovpas.footbot.server.frontend.BaseView

interface AssociationsView : BaseView {
    var loading: Boolean
    var refresh: Boolean
    fun showList(associations: List<Association>)
}
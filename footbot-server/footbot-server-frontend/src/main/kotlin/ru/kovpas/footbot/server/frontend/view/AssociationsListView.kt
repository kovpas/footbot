package ru.kovpas.footbot.server.frontend.view

import kotlinx.html.UL
import libraries.react.material.Input
import react.RBuilder
import react.ReactElement
import react.dom.*
import ru.kovpas.footbot.core.model.Association

fun RBuilder.associationsListView(
    associations: List<Association>,
    onFilterChange: (text: String) -> Unit
): ReactElement? =
    div {
        child(Input::class) {
            attrs.onChange = {
                onFilterChange(it.target.asDynamic().value as String)
            }
        }
        ul(classes = "collection") {
            for (a in associations) {
                renderAssociation(a)
            }
        }
    }

fun RDOMBuilder<UL>.renderAssociation(association: Association) =
    li(classes = "collection-item") {
        a(href = association.url) {
            +association.name
            div(classes = "secondary-content") {
                +association.abbreviation
            }
        }
    }

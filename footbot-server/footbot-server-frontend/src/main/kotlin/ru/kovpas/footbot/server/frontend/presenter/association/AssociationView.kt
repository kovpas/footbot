package ru.kovpas.footbot.server.frontend.presenter.association

import ru.kovpas.footbot.core.model.Association
import ru.kovpas.footbot.core.model.League
import ru.kovpas.footbot.server.frontend.BaseView

interface AssociationView: BaseView {
    var loading: Boolean
    var refresh: Boolean
    fun showAssociation(association: Association)
}
package ru.kovpas.footbot.server.frontend.repository

import kotlinx.coroutines.coroutineScope
import ru.kovpas.footbot.core.model.Association
import ru.kovpas.footbot.core.model.AssociationType
import ru.kovpas.footbot.core.model.League
import ru.kovpas.footbot.core.model.LeagueType
import ru.kovpas.footbot.server.frontend.utils.get

class AssociationRepository {
    private var cachedAssociation: Association? = null
    private var cachedLeagues: List<League>? = null

    suspend fun getAssociation(abbreviation: String): Association = coroutineScope {
        val result = cachedAssociation
        if (result != null) {
            result
        } else {
            val associationData= JSON.parse<AssociationData>(get("api/association/$abbreviation"))
            val association = transformAssociationsDataToAssociation(associationData)
            cachedAssociation = association
            association
        }
    }

    suspend fun getLeagues(association: Association): List<League> = coroutineScope {
        val result = cachedLeagues
        if (result != null) {
            result
        } else {
            val leagues = JSON.parse<Array<League>>(get("api/association/${association.abbreviation}/leagues")).toList()
            cachedLeagues = leagues
            leagues
        }
    }

    suspend fun scrapeLeagues(association: Association): List<League> = coroutineScope {
        JSON.parse<Array<String>>(get("api/scrape/leagues")).toList().map {
            console.log(it)
            League(0, it, LeagueType.CLUB, association)
        }
    }

    private fun transformAssociationsDataToAssociation(associationData: AssociationData): Association {
        return Association(
                id = associationData.id,
                abbreviation = associationData.abbreviation,
                name = associationData.name,
                type = AssociationType.valueOf(associationData.type),
                parentAssociations = listOf(),
                country = associationData.country
        )
    }
}

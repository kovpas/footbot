package ru.kovpas.footbot.server.frontend.presenter.association

import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import ru.kovpas.footbot.core.model.Association
import ru.kovpas.footbot.core.model.League
import ru.kovpas.footbot.server.frontend.presenter.BasePresenter
import ru.kovpas.footbot.server.frontend.repository.AssociationRepository
import kotlin.coroutines.CoroutineContext

class AssociationPresenter(
    private val uiContext: CoroutineContext,
    private val view: AssociationView,
    private val associationRepository: AssociationRepository,
    private val abbreviation: String
) : BasePresenter() {

    private var visibleAssociation: Association? = null
    private var scrapedLeagues: List<League>? = null

    override fun onCreate() {
        console.log(abbreviation)
        view.loading = true

        refreshAssociation()
    }

    private fun refreshAssociation() {
        jobs += GlobalScope.launch(uiContext) {
            try {
                val association = associationRepository.getAssociation(abbreviation)
                association.leagues = associationRepository.getLeagues(association).toMutableList()
                if (association == visibleAssociation) return@launch
                visibleAssociation = association

                view.showAssociation(association)
            } catch (e: Throwable) {
                view.showError(e)
            } finally {
                view.refresh = false
                view.loading = false
            }
        }
    }

    fun scrapeLeagues() {
        jobs += GlobalScope.launch(uiContext) {
            try {
                val association = visibleAssociation ?: return@launch
                association.leagues = associationRepository.scrapeLeagues(association).toMutableList()
                console.log(association)

                view.showAssociation(association)
            } catch (e: Throwable) {
                view.showError(e)
            }
        }
    }
}
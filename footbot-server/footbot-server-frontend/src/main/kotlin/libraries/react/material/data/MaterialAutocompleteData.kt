package libraries.react.material.data

import react.dom.WithClassName

// INTERFACES

interface AutocompleteProps : WithClassName {
    var data: Map<String, String>
}

// IMPLEMENTATIONS

data class AutocompleteData(
    override var data: Map<String, String> = mapOf<String, String>(),
    override var className: String? = ""
) : AutocompleteProps

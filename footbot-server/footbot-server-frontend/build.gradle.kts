import org.gradle.api.tasks.Copy
import org.gradle.kotlin.dsl.*
import org.jetbrains.kotlin.gradle.dsl.Coroutines
import org.jetbrains.kotlin.gradle.frontend.KotlinFrontendExtension
import org.jetbrains.kotlin.gradle.frontend.npm.NpmExtension
import org.jetbrains.kotlin.gradle.frontend.webpack.WebPackExtension
import org.jetbrains.kotlin.gradle.tasks.Kotlin2JsCompile
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

description = "Footbot server frontend"

group = "ru.kovpas.footbot.server.frontend"
version = "0.1.0"

buildscript {
    val kotlin_frontend_plugin_version: String by project
    dependencies {
        classpath("org.jetbrains.kotlin", "kotlin-frontend-plugin", kotlin_frontend_plugin_version)
    }
}

plugins {
    id("kotlin2js")
    id("kotlin-dce-js")
    id("org.jetbrains.kotlin.frontend")
}

repositories {
    maven { url = uri("http://dl.bintray.com/kotlin/kotlinx.html/") }
    maven { url = uri("http://dl.bintray.com/kotlin/kotlin-js-wrappers/") }
}

val kotlinx_html_version: String by project
val kotlin_version: String by project
val coroutines_version: String by project
val extensions_version: String by project
val react_version: String by project
val react_router_version: String by project
val css_version: String by project

dependencies {
    implementation(kotlin("stdlib-js"))
    implementation("org.jetbrains.kotlinx", "kotlinx-html-js", kotlinx_html_version)
    implementation("org.jetbrains.kotlinx", "kotlinx-coroutines-core-js", coroutines_version)

    implementation("org.jetbrains", "kotlin-extensions", extensions_version)
    implementation("org.jetbrains", "kotlin-react", react_version)
    implementation("org.jetbrains", "kotlin-react-dom", react_version)
    implementation("org.jetbrains", "kotlin-react-router-dom", react_router_version)
    implementation("org.jetbrains", "kotlin-css-js", css_version)
    implementation("org.jetbrains", "kotlin-styled", css_version)

    implementation(project(":footbot-core:footbot-core-js-model"))

    testImplementation("org.jetbrains.kotlin", "kotlin-test-js", kotlin_version)
}

kotlinFrontend {
    sourceMaps = true

    npm {
        dependency("react")
        dependency("react-dom")
        dependency("react-router")
        dependency("react-router-dom")
        dependency("marked")
        dependency("jquery")
        dependency("react-markdown")
        dependency("react-materialize")

        dependency("css-loader")
        dependency("style-loader")
        dependency("file-loader")

        devDependency("karma")
    }

    bundle<WebPackExtension>("webpack") {
        (this as WebPackExtension).apply {
            port = 8080
            publicPath = "/frontend/"
            proxyUrl = "http://[::1]:9000"
        }
    }

    define("PRODUCTION", true)
}

val compileTestKotlin2Js: Kotlin2JsCompile by tasks
compileTestKotlin2Js.kotlinOptions {
    metaInfo = true
    outputFile = "${project.buildDir.path}/js-tests/${project.name}-tests.js"
    sourceMap = true
    moduleKind = "commonjs"
    main = "call"
}

val compileKotlin2Js: Kotlin2JsCompile by tasks
compileKotlin2Js.kotlinOptions {
    metaInfo = true
    outputFile = "${project.buildDir.path}/js/${project.name}.js"
    sourceMap = true
    sourceMapEmbedSources = "always"
    moduleKind = "commonjs"
    main = "call"
    languageVersion = "1.3"
    apiVersion = "1.3"
}

val copyResources by tasks.registering(Copy::class) {
    includeEmptyDirs = false
    from(sourceSets["main"].resources.srcDirs)
    into(file(buildDir.path + "/kotlin-js-min/main"))
}

afterEvaluate {
    tasks["webpack-bundle"].dependsOn(copyResources)
    tasks["webpack-run"].dependsOn(copyResources)
    tasks["webpack-bundle"].dependsOn("runDceKotlinJs")
    tasks["webpack-run"].dependsOn("runDceKotlinJs")
}

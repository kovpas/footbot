description = "Footbot scrapers"

group = "ru.kovpas.footbot.scraper"
version = "0.1.0"

plugins {
    kotlin("jvm")
}

dependencies {
    compile(project(":footbot-scraper:footbot-scraper-sportsru"))
}
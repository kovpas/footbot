description = "Sports.ru scraper"

group = "ru.kovpas.footbot.scraper.sportsru"
version = "0.1.0"

plugins {
    kotlin("jvm")
}

dependencies {
    compile(kotlin("stdlib"))
    compile(project(":footbot-core:footbot-core-jvm-model"))
    compile(project(":footbot-scraper:footbot-scraper-core"))
    compile(project(":footbot-scraper:footbot-scraper-sportsru-connector"))
}
package ru.kovpas.footbot.scraper

import ru.kovpas.footbot.core.model.Association
import ru.kovpas.footbot.core.model.League
import ru.kovpas.footbot.core.model.LeagueType
import java.net.URL

class SportsruScraper(private val htmlScraper: HtmlScraper) {
    companion object {
        private val BASE_URL = URL("https://sports.ru")
        private val ASSOCIATIONS_URL = URL(BASE_URL, "/stat/football/center/")
        private val LEAGUES_URL = URL(BASE_URL, "/stat/football/russia/")
    }

    suspend fun leagues(association: Association): List<League> {
        val doc = htmlScraper.toDoc(LEAGUES_URL)
        val leaguesLinks = doc.select("div.match-statistic a")

        return leaguesLinks.map { leagueElement ->
            League(title = leagueElement.text(), type = LeagueType.CLUB, association = association)
        }
    }
}
package ru.kovpas.footbot.scraper.sportsru.connector

import io.ebean.Ebean
import io.ebean.annotation.Index
import ru.kovpas.footbot.scraper.sportsru.connector.finder.*
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "sportsru_associations")
class SportsruAssociationsConnector(coreId: Long, sportsruKey: String) : BaseConnector(coreId, sportsruKey) {
    companion object Find : SportsruAssociationsConnectorFinder()
}

@Entity
@Table(name = "sportsru_countries")
class SportsruCountriesConnector(coreId: Long, sportsruKey: String) : BaseConnector(coreId, sportsruKey) {
    companion object Find : SportsruCountriesConnectorFinder() {
        fun deleteAll(): Int = Ebean.deleteAll(SportsruCountriesConnector.all())
    }
}

@Entity
@Table(name = "sportsru_leagues")
class SportsruLeaguesConnector(coreId: Long, sportsruKey: String) : BaseConnector(coreId, sportsruKey) {
    companion object Find : SportsruLeaguesConnectorFinder() {
        fun deleteAll(): Int = Ebean.deleteAll(SportsruLeaguesConnector.all())
    }
}

@Entity
@Table(name = "sportsru_teams")
class SportsruTeamsConnector(coreId: Long, sportsruKey: String) : BaseConnector(coreId, sportsruKey) {
    companion object Find : SportsruTeamsConnectorFinder() {
        fun deleteAll(): Int = Ebean.deleteAll(SportsruTeamsConnector.all())
    }
}

@Entity
@Table(name = "sportsru_seasons")
class SportsruSeasonsConnector(coreId: Long, sportsruKey: String) : BaseConnector(coreId, sportsruKey) {
    companion object Find : SportsruSeasonsConnectorFinder() {
        fun deleteAll(): Int = Ebean.deleteAll(SportsruSeasonsConnector.all())
    }
}

@Entity
@Table(name = "sportsru_rounds")
class SportsruRoundsConnector(coreId: Long, sportsruKey: String) : BaseConnector(coreId, sportsruKey) {
    companion object Find : SportsruRoundsConnectorFinder() {
        fun deleteAll(): Int = Ebean.deleteAll(SportsruRoundsConnector.all())
    }
}

@Entity
@Table(name = "sportsru_events")
class SportsruEventsConnector(coreId: Long, sportsruKey: String) : BaseConnector(coreId, sportsruKey) {
    companion object Find : SportsruEventsConnectorFinder() {
        fun deleteAll(): Int = Ebean.deleteAll(SportsruEventsConnector.all())
    }
}

@Entity
@Table(name = "sportsru_groups")
class SportsruGroupsConnector(coreId: Long, sportsruKey: String) : BaseConnector(coreId, sportsruKey) {
    companion object Find : SportsruGroupsConnectorFinder() {
        fun deleteAll(): Int = Ebean.deleteAll(SportsruGroupsConnector.all())
    }
}

@Entity
@Table(name = "sportsru_games")
class SportsruGamesConnector(coreId: Long, sportsruKey: String) : BaseConnector(coreId, sportsruKey) {
    companion object Find : SportsruGamesConnectorFinder() {
        fun deleteAll(): Int = Ebean.deleteAll(SportsruGamesConnector.all())
    }
}

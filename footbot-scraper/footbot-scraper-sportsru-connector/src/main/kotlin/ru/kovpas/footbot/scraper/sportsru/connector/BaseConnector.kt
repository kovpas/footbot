package ru.kovpas.footbot.scraper.sportsru.connector

import io.ebean.Model
import io.ebean.annotation.Index
import io.ebean.annotation.JsonIgnore
import io.ebean.annotation.WhenCreated
import io.ebean.annotation.WhenModified
import java.time.Instant
import javax.persistence.Column
import javax.persistence.Id
import javax.persistence.MappedSuperclass
import javax.persistence.Version

@MappedSuperclass
open class BaseConnector(
    @Index
    @Column(unique = true)
    val coreId: Long,

    @Index
    @Column(length = 150, unique = true)
    val sportsruKey: String
) : Model() {

    @Id
    val id: Long = 0

    @Version
    @JsonIgnore
    var version: Long = 0

    @WhenCreated
    @JsonIgnore
    lateinit var whenCreated: Instant

    @WhenModified
    @JsonIgnore
    lateinit var whenModified: Instant


}
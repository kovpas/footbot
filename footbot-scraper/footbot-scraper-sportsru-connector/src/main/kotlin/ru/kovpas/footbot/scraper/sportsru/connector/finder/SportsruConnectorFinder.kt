package ru.kovpas.footbot.scraper.sportsru.connector.finder

import io.ebean.Ebean
import io.ebean.Finder
import ru.kovpas.footbot.scraper.sportsru.connector.*
import ru.kovpas.footbot.scraper.sportsru.connector.query.*

open class SportsruAssociationsConnectorFinder :
    Finder<Long, SportsruAssociationsConnector>(SportsruAssociationsConnector::class.java) {
    fun coreIdFor(sportsruKey: String): Long? =
        QSportsruAssociationsConnector().sportsruKey.equalTo(sportsruKey).findOne()?.coreId

    fun sportsruKeyFor(coreId: Long): String? =
        QSportsruAssociationsConnector().coreId.equalTo(coreId).findOne()?.sportsruKey

    fun deleteAll(): Int = Ebean.deleteAll(SportsruAssociationsConnector.all())
}

open class SportsruCountriesConnectorFinder :
    Finder<Long, SportsruCountriesConnector>(SportsruCountriesConnector::class.java) {
    fun coreIdFor(sportsruKey: String): Long? =
        QSportsruCountriesConnector().sportsruKey.equalTo(sportsruKey).findOne()?.coreId

    fun sportsruKeyFor(coreId: Long): String? =
        QSportsruCountriesConnector().coreId.equalTo(coreId).findOne()?.sportsruKey
}

open class SportsruLeaguesConnectorFinder :
    Finder<Long, SportsruLeaguesConnector>(SportsruLeaguesConnector::class.java) {
    fun coreIdFor(sportsruKey: String): Long? =
        QSportsruLeaguesConnector().sportsruKey.equalTo(sportsruKey).findOne()?.coreId

    fun sportsruKeyFor(coreId: Long): String? =
        QSportsruLeaguesConnector().coreId.equalTo(coreId).findOne()?.sportsruKey
}

open class SportsruTeamsConnectorFinder :
    Finder<Long, SportsruTeamsConnector>(SportsruTeamsConnector::class.java) {
    fun coreIdFor(sportsruKey: String): Long? =
        QSportsruTeamsConnector().sportsruKey.equalTo(sportsruKey).findOne()?.coreId

    fun sportsruKeyFor(coreId: Long): String? =
        QSportsruTeamsConnector().coreId.equalTo(coreId).findOne()?.sportsruKey
}

open class SportsruSeasonsConnectorFinder :
    Finder<Long, SportsruSeasonsConnector>(SportsruSeasonsConnector::class.java) {
    fun coreIdFor(sportsruKey: String): Long? =
        QSportsruSeasonsConnector().sportsruKey.equalTo(sportsruKey).findOne()?.coreId

    fun sportsruKeyFor(coreId: Long): String? =
        QSportsruSeasonsConnector().coreId.equalTo(coreId).findOne()?.sportsruKey
}

open class SportsruRoundsConnectorFinder :
    Finder<Long, SportsruRoundsConnector>(SportsruRoundsConnector::class.java) {
    fun coreIdFor(sportsruKey: String): Long? =
        QSportsruRoundsConnector().sportsruKey.equalTo(sportsruKey).findOne()?.coreId

    fun sportsruKeyFor(coreId: Long): String? =
        QSportsruRoundsConnector().coreId.equalTo(coreId).findOne()?.sportsruKey
}

open class SportsruEventsConnectorFinder :
    Finder<Long, SportsruEventsConnector>(SportsruEventsConnector::class.java) {
    fun coreIdFor(sportsruKey: String): Long? =
        QSportsruEventsConnector().sportsruKey.equalTo(sportsruKey).findOne()?.coreId

    fun sportsruKeyFor(coreId: Long): String? =
        QSportsruEventsConnector().coreId.equalTo(coreId).findOne()?.sportsruKey
}

open class SportsruGroupsConnectorFinder :
    Finder<Long, SportsruGroupsConnector>(SportsruGroupsConnector::class.java) {
    fun coreIdFor(sportsruKey: String): Long? =
        QSportsruGroupsConnector().sportsruKey.equalTo(sportsruKey).findOne()?.coreId

    fun sportsruKeyFor(coreId: Long): String? =
        QSportsruGroupsConnector().coreId.equalTo(coreId).findOne()?.sportsruKey
}

open class SportsruGamesConnectorFinder :
    Finder<Long, SportsruGamesConnector>(SportsruGamesConnector::class.java) {
    fun coreIdFor(sportsruKey: String): Long? =
        QSportsruGamesConnector().sportsruKey.equalTo(sportsruKey).findOne()?.coreId

    fun sportsruKeyFor(coreId: Long): String? =
        QSportsruGamesConnector().coreId.equalTo(coreId).findOne()?.sportsruKey
}

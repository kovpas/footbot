package ru.kovpas.footbot.scraper

import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.mockk
import kotlinx.coroutines.runBlocking
import java.net.URL
import kotlin.test.Test
import kotlin.test.assertEquals

open class HtmlScraperTest {
    private val httpClient = mockk<HttpClient>()
    private val sutScraper = HtmlScraper(httpClient)
    private val url = URL("http://test_url.com")

    @Test
    fun testParseSmallDocument() = runBlocking {
        coEvery { httpClient.get(url) } returns javaClass.getResource("/small.html").readText()
        val testDoc = sutScraper.toDoc(url)

        coVerify {
            sutScraper.toDoc(url)

            assertEquals(testDoc.children().count(), 1)
            assertEquals(testDoc.select("html head").count(), 1)
            assertEquals(testDoc.select("html body").count(), 1)
            assertEquals(testDoc.select("no-html").count(), 0)
        }
    }

    @Test
    fun testParseBigDocument() = runBlocking {
        coEvery { httpClient.get(url) } returns javaClass.getResource("/rpl.html").readText()
        val testDoc = sutScraper.toDoc(url)

        coVerify {
            sutScraper.toDoc(url)

            assertEquals(testDoc.select("div#middle_col").count(), 1)
        }
    }

    @Test
    fun testInitializeEmptyHtml() = runBlocking {
        coEvery { httpClient.get(url) } returns javaClass.getResource("/empty.html").readText()
        val testDoc = sutScraper.toDoc(url)

        coVerify {
            sutScraper.toDoc(url)

            assertEquals(testDoc.select("html head").count(), 1)
            assertEquals(testDoc.select("html body").count(), 1)
        }
    }
}

package ru.kovpas.footbot.scraper

import kotlinx.coroutines.coroutineScope
import org.jsoup.Jsoup
import org.jsoup.nodes.Document
import java.net.URL

class HtmlScraper(client: HttpClient) {
    private val httpClient: HttpClient = client

    suspend fun toDoc(url: URL): Document = coroutineScope {
        val htmlString = httpClient.get(url)
        val document = toDoc(htmlString)
        document
    }

    private fun toDoc(html: String): Document {
        val document = Jsoup.parse(html)
        val settings = document.outputSettings().prettyPrint(true)

        document.outputSettings(settings)

        return document
    }
}
package ru.kovpas.footbot.scraper

import kotlinx.coroutines.async
import kotlinx.coroutines.coroutineScope
import java.net.URL

class HttpClient {
    suspend fun get(url: URL): String = coroutineScope {
        val urlText = async {
            url.readText()
        }

        urlText.await()
    }
}
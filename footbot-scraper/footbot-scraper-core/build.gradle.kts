description = "Base scraper classes and interfaces"

group = "ru.kovpas.footbot.scraper.core"
version = "0.1.0"

plugins {
    kotlin("jvm")
}

val coroutines_version: String by project
val jsoup_version: String by project
val junit_version: String by project
val kotlin_version: String by project
val mockk_version: String by project

dependencies {
    compile(kotlin("stdlib"))

    implementation("org.jsoup", "jsoup", jsoup_version)
    implementation("org.jetbrains.kotlinx", "kotlinx-coroutines-core", coroutines_version)

    testCompile("org.junit.jupiter", "junit-jupiter-api", junit_version)
    testCompile("org.junit.jupiter", "junit-jupiter-params", junit_version)
    testRuntime("org.junit.jupiter", "junit-jupiter-engine", junit_version)
    testCompile("org.jetbrains.kotlin", "kotlin-test-junit5", kotlin_version)

    testImplementation("io.mockk", "mockk", mockk_version)
}

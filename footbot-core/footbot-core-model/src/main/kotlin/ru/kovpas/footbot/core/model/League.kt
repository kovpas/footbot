package ru.kovpas.footbot.core.model

expect class League(id: Long = 0,
                    title: String,
                    type: LeagueType,
                    association: Association) {
    val id: Long
    val title: String
    val type: LeagueType
    val association: Association

    fun events(): List<Event>
}

fun League.seasons(): List<Season> = events().map { it.season }

enum class LeagueType {
    CLUB,
    NATIONAL
}
package ru.kovpas.footbot.core.model

expect class Group(
    id: Long = 0,
    pos: Long,
    title: String,
    event: Event
) {
    val id: Long
    val pos: Long
    val title: String
    val event: Event

    fun teams(): List<Team>
    fun games(): List<Game>
}

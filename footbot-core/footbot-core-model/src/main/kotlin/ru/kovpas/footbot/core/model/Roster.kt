package ru.kovpas.footbot.core.model

expect class Roster(
    id: Long = 0,
    person: Person,
    team: Team,
    event: Event
) {
    val id: Long
    val person: Person
    val team: Team
    val event: Event
}
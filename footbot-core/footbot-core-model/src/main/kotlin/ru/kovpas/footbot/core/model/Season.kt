package ru.kovpas.footbot.core.model

expect class Season(
    id: Long = 0,
    title: String
) {
    val id: Long
    val title: String

    fun events(): List<Event>
}
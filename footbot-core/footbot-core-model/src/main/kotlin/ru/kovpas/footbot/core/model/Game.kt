package ru.kovpas.footbot.core.model

expect class Game(
    id: Long = 0,
    pos: Long,
    round: Round,
    group: Group?,
    team1: Team,
    team2: Team,
    startTimestamp: Long,
    endTimestamp: Long,
    startTimestamp2: Long? = null, // start timestamp if postponed
    startTimestamp3: Long? = null, // start timestamp if postponed twice
    postponed: Boolean = false,
    isHome: Boolean = true,
    score1: Int? = null,
    score2: Int? = null,
    score1et: Int? = null,  // extratime - team 1
    score2et: Int? = null,  // extratime - team 2
    score1p: Int? = null,   // penalty - team 1
    score2p: Int? = null,   // penalty - team 2
    score1i: Int? = null,   // first half - team 1
    score2i: Int? = null,   // first half - team 2
    score1ii: Int? = null,  // second half - team 1
    score2ii: Int? = null,  // second half - team 2
    prevGame: Game? = null,
    nextGame: Game? = null
) {
    val id: Long
    val pos: Long
    val round: Round
    val group: Group?
    val team1: Team
    val team2: Team
    val startTimestamp: Long
    val endTimestamp: Long
    val startTimestamp2: Long?
    val startTimestamp3: Long?
    val postponed: Boolean
    val isHome: Boolean
    val score1: Int?
    val score2: Int?
    val score1et: Int?
    val score2et: Int?
    val score1p: Int?
    val score2p: Int?
    val score1i: Int?
    val score2i: Int?
    val score1ii: Int?
    val score2ii: Int?
    val prevGame: Game?
    val nextGame: Game?

    fun goals(): List<Goal>
    fun over(): Boolean
    fun winner(): WinnerType?
    fun winner90(): WinnerType?
}


enum class WinnerType {
    HOME,
    DRAW,
    AWAY
}
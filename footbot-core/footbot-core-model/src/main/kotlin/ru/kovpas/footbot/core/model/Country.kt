package ru.kovpas.footbot.core.model

expect class Country(id: Long = 0,
                     code: String,
                     code3: String,
                     name: String,
                     continent: Continent) {
    val id: Long
    val code: String
    val code3: String
    val name: String
    val continent: Continent

    fun associations(): List<Association>
}

fun Country.teams(): List<Team> = associations().map { it.teams() }.flatten()
fun Country.leagues(): List<League> = associations().map { it.leagues() }.flatten()

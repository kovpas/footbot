package ru.kovpas.footbot.core.model

expect class Association(id: Long = 0,
                         abbreviation: String,
                         name: String,
                         type: AssociationType,
                         parentAssociations: List<Association> = ArrayList(),
                         country: Country? = null) {
    val id: Long
    val abbreviation: String
    val name: String
    val type: AssociationType
    val parentAssociations: List<Association>
    val country: Country?

    fun childAssociations(): List<Association>
    fun leagues(): List<League>
    fun teams(): List<Team>
}

enum class AssociationType {
    NATIONAL,
    CONTINENTAL,
    INTERCONTINENTAL
}
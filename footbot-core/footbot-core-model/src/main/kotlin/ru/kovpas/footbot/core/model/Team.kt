package ru.kovpas.footbot.core.model

expect class Team(
    id: Long = 0,
    title: String,
    type: TeamType,
    association: Association
) {
    val id: Long
    val title: String
    val type: TeamType
    val association: Association

    fun events(): List<Event>
}

enum class TeamType {
    CLUB,
    NATIONAL
}
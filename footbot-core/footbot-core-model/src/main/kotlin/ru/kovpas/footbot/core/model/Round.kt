package ru.kovpas.footbot.core.model

expect class Round(
    id: Long = 0,
    pos: Long,
    title: String,
    type: RoundType = RoundType.GROUP,
    event: Event,
    startTimestamp: Long, // date without time
    endTimestamp: Long // date without time
) {
    val id: Long
    val pos: Long
    val title: String
    val type: RoundType
    val event: Event
    val startTimestamp: Long
    val endTimestamp: Long

    fun games(): List<Game>
}

enum class RoundType {
    GROUP,
    KNOCKOUT
}
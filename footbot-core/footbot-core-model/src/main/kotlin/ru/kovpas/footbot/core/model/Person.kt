package ru.kovpas.footbot.core.model

expect class Person(
    id: Long = 0,
    name: String,
    country: Country
) {
    val id: Long
    val name: String
    val country: Country

    fun goals(): List<Goal>
}
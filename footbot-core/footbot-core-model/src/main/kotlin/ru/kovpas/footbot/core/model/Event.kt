package ru.kovpas.footbot.core.model

expect class Event(
    id: Long = 0,
    league: League,
    season: Season,
    startTimestamp: Long,
    endTimestamp: Long
) {
    val id: Long
    val league: League
    val season: Season
    val startTimestamp: Long
    val endTimestamp: Long

    fun rounds(): List<Round>
    fun groups(): List<Group>
    fun teams(): List<Team>
}

fun Event.games(): List<Game> = rounds().map { it.games() }.flatten().sortedBy { it.pos }

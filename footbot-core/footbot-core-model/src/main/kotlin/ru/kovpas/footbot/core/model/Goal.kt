package ru.kovpas.footbot.core.model

expect class Goal(
    id: Long = 0,
    person: Person,
    game: Game,
    team: Team,
    minute: Int,
    offset: Int = 0,
    type: GoalType = GoalType.GAME,
    ownGoal: Boolean = false
) {
    val id: Long
    val person: Person
    val game: Game
    val team: Team
    val minute: Int
    val offset: Int
    val type: GoalType
    val ownGoal: Boolean
}


enum class GoalType {
    GAME,
    PENALTY
}
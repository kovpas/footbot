package ru.kovpas.footbot.core.model

expect class Continent(id: Long = 0,
                       code: String,
                       name: String) {
    val id: Long
    val code: String
    val name: String
    var countries: MutableList<Country>
}

fun Continent.teams(): List<Team> = countries.map { it.teams() }.flatten()
fun Continent.leagues(): List<League> = countries.map { it.leagues() }.flatten()

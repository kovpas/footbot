description = "Footbot base model"

group = "ru.kovpas.footbot.bot"
version = "0.1.0"

buildscript {
    val kotlin_version: String by project
    dependencies {
        classpath("org.jetbrains.kotlin", "kotlin-serialization", kotlin_version)
    }
}

plugins {
    id("kotlin-platform-common")
    id("kotlinx-serialization")
}

val kotlin_version: String by project
val kotlinx_html_version: String by project
val serialization_version: String by project

dependencies {
    compile(kotlin("stdlib"))

    testCompile("org.jetbrains.kotlin", "kotlin-test-annotations-common", kotlin_version)
    testCompile("org.jetbrains.kotlin", "kotlin-test-common", kotlin_version)
}
package ru.kovpas.footbot.core.model

import kotlin.js.Date

actual class Game actual constructor(
    actual val id: Long,
    actual val pos: Long,
    actual val round: Round,
    actual val group: Group?,
    actual val team1: Team,
    actual val team2: Team,
    actual val startTimestamp: Long,
    actual val endTimestamp: Long,
    actual val startTimestamp2: Long?,
    actual val startTimestamp3: Long?,
    actual val postponed: Boolean,
    actual val isHome: Boolean,
    actual val score1: Int?,
    actual val score2: Int?,
    actual val score1et: Int?,
    actual val score2et: Int?,
    actual val score1p: Int?,
    actual val score2p: Int?,
    actual val score1i: Int?,
    actual val score2i: Int?,
    actual val score1ii: Int?,
    actual val score2ii: Int?,
    actual val prevGame: Game?,
    actual val nextGame: Game?
) {
    actual fun goals(): List<Goal> = listOf()
    actual fun over(): Boolean = endTimestamp <= Date.now()
    actual fun winner(): WinnerType? = WinnerType.DRAW
    actual fun winner90(): WinnerType? = WinnerType.DRAW
}
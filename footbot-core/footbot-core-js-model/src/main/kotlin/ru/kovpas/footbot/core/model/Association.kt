package ru.kovpas.footbot.core.model

actual class Association actual constructor(
    actual val id: Long,
    actual val abbreviation: String,
    actual val name: String,
    actual val type: AssociationType,
    actual val parentAssociations: List<Association>,
    actual val country: Country?
) {
    var childAssociations: MutableList<Association> = mutableListOf()
    actual fun childAssociations(): List<Association> = childAssociations

    var leagues: MutableList<League> = mutableListOf()
    actual fun leagues(): List<League> = leagues

    var teams: MutableList<Team> = mutableListOf()
    actual fun teams(): List<Team> = teams

    val url: String
        get() = "#/association/$abbreviation"
}
package ru.kovpas.footbot.core.model

actual class League actual constructor(
    actual val id: Long,
    actual val title: String,
    actual val type: LeagueType,
    actual val association: Association
) {
    actual fun events(): List<Event> = listOf()
}
package ru.kovpas.footbot.core.model

actual class Goal actual constructor(
    actual val id: Long,
    actual val person: Person,
    actual val game: Game,
    actual val team: Team,
    actual val minute: Int,
    actual val offset: Int,
    actual val type: GoalType,
    actual val ownGoal: Boolean
)
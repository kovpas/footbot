package ru.kovpas.footbot.core.model

actual class Event actual constructor(
    actual val id: Long,
    actual val league: League,
    actual val season: Season,
    actual val startTimestamp: Long,
    actual val endTimestamp: Long
) {
    var rounds: MutableList<Round> = mutableListOf()
    actual fun rounds(): List<Round> = rounds

    var groups: MutableList<Group> = mutableListOf()
    actual fun groups(): List<Group> = groups

    var teams: MutableList<Team> = mutableListOf()
    actual fun teams(): List<Team> = teams
}
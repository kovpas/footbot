package ru.kovpas.footbot.core.model

actual class Roster actual constructor(
    actual val id: Long,
    actual val person: Person,
    actual val team: Team,
    actual val event: Event
)
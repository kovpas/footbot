package ru.kovpas.footbot.core.model

actual class Round actual constructor(
    actual val id: Long,
    actual val pos: Long,
    actual val title: String,
    actual val type: RoundType,
    actual val event: Event,
    actual val startTimestamp: Long,
    actual val endTimestamp: Long
) {
    actual fun games(): List<Game> = listOf()
}
package ru.kovpas.footbot.core.model

actual class Season actual constructor(
    actual val id: Long,
    actual val title: String
) {
    actual fun events(): List<Event> = listOf()
}
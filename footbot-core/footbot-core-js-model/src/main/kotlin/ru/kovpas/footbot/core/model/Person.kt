package ru.kovpas.footbot.core.model

actual class Person actual constructor(
    actual val id: Long,
    actual val name: String,
    actual val country: Country
) {
    actual fun goals(): List<Goal> = listOf()
}
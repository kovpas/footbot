package ru.kovpas.footbot.core.model

actual class Country actual constructor(
    actual val id: Long,
    actual val code: String,
    actual val code3: String,
    actual val name: String,
    actual val continent: Continent
) {
    var associations: MutableList<Association> = mutableListOf()
    actual fun associations(): List<Association> = associations
}
package ru.kovpas.footbot.core.model

actual class Continent actual constructor(
    actual val id: Long,
    actual val code: String,
    actual val name: String
) {
    actual var countries: MutableList<Country> = mutableListOf()
}
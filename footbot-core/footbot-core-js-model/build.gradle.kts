import org.jetbrains.kotlin.gradle.tasks.Kotlin2JsCompile

description = "Footbot bot"

group = "ru.kovpas.footbot.bot"
version = "0.1.0"

buildscript {
    val kotlin_version: String by project
    dependencies {
        classpath("org.jetbrains.kotlin", "kotlin-serialization", kotlin_version)
    }
}

plugins {
    id("kotlin-platform-js")
    id("kotlinx-serialization")
}

val kotlin_version: String by project
val kotlinx_html_version: String by project
val serialization_version: String by project

dependencies {
    implementation(kotlin("stdlib-js"))
    expectedBy(project(":footbot-core:footbot-core-model"))

    testImplementation("org.jetbrains.kotlin", "kotlin-test-js", kotlin_version)
}

val compileKotlin2Js: Kotlin2JsCompile by tasks
compileKotlin2Js.kotlinOptions {
    sourceMap = true
    moduleKind = "commonjs"
}

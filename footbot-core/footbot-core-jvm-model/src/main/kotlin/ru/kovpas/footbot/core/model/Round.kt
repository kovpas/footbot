package ru.kovpas.footbot.core.model

import io.ebean.Ebean
import ru.kovpas.footbot.core.model.finder.RoundFinder
import javax.persistence.*

@Entity
@Table(name = "rounds")
actual class Round actual constructor(
    @Id
    actual val id: Long,

    @Column actual val pos: Long,

    @Column(length = 150)
    actual val title: String,

    @Enumerated(EnumType.STRING)
    actual val type: RoundType,

    @ManyToOne(cascade = [CascadeType.REFRESH, CascadeType.MERGE, CascadeType.PERSIST])
    actual val event: Event,

    @Column(columnDefinition = "datetime")
    actual val startTimestamp: Long,

    @Column(columnDefinition = "datetime")
    actual val endTimestamp: Long
) {

    @OneToMany(cascade = [CascadeType.ALL])
    @OrderBy("pos")
    private var games: List<Game> = listOf()
    actual fun games(): List<Game> = games

    companion object Find : RoundFinder() {
        fun deleteAll(): Int {
            return Ebean.deleteAll(Round.all())
        }
    }

}
package ru.kovpas.footbot.core.model.finder

import io.ebean.Finder
import ru.kovpas.footbot.core.model.Event
import ru.kovpas.footbot.core.model.query.QEvent

open class EventFinder: Finder<Long, Event>(Event::class.java) {
    val alias = QEvent._alias
}
package ru.kovpas.footbot.core.model.finder

import io.ebean.Finder
import ru.kovpas.footbot.core.model.Group
import ru.kovpas.footbot.core.model.query.QGroup

open class GroupFinder: Finder<Long, Group>(Group::class.java) {
    val alias = QGroup._alias
}
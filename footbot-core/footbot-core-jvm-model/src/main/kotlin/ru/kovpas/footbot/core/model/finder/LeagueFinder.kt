package ru.kovpas.footbot.core.model.finder

import io.ebean.Finder
import ru.kovpas.footbot.core.model.League
import ru.kovpas.footbot.core.model.query.QLeague

open class LeagueFinder : Finder<Long, League>(League::class.java) {
    val alias = QLeague._alias
}

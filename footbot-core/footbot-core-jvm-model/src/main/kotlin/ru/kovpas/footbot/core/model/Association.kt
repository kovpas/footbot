package ru.kovpas.footbot.core.model

import io.ebean.Ebean
import io.ebean.annotation.DbForeignKey
import io.ebean.annotation.Transactional
import ru.kovpas.footbot.core.model.finder.AssociationFinder
import javax.persistence.*

@Entity
@Table(name = "associations")
actual class Association actual constructor(
    @Id
    actual val id: Long,

    @Column(length = 20, unique = true)
    actual val abbreviation: String,

    @Column(length = 150)
    actual val name: String,

    @Enumerated(EnumType.STRING)
    actual val type: AssociationType,

    @ManyToMany(mappedBy = "childAssociations", cascade = [CascadeType.REFRESH, CascadeType.MERGE, CascadeType.PERSIST])
    @DbForeignKey(onDelete = io.ebean.annotation.ConstraintMode.CASCADE)
    actual val parentAssociations: List<Association>,

    @ManyToOne(cascade = [CascadeType.REFRESH, CascadeType.MERGE, CascadeType.PERSIST])
    @DbForeignKey(onDelete = io.ebean.annotation.ConstraintMode.CASCADE)
    actual val country: Country?
) : BaseModel() {
    @ManyToMany(cascade = [CascadeType.ALL])
    @JoinTable(
        name = "associations_associations",
        joinColumns = [JoinColumn(name = "parent_association_id", referencedColumnName = "id")],
        inverseJoinColumns = [JoinColumn(name = "child_association_id", referencedColumnName = "id")]
    )
    private var childAssociations: List<Association> = listOf()

    actual fun childAssociations(): List<Association> = childAssociations

    @OneToMany(mappedBy = "association", cascade = [CascadeType.ALL])
    private val leagues: List<League> = listOf()

    actual fun leagues(): List<League> = leagues

    @OneToMany(mappedBy = "association", cascade = [CascadeType.ALL])
    private val teams: List<Team> = listOf()

    actual fun teams(): List<Team> = teams

    companion object Find : AssociationFinder() {
        fun deleteAll(): Int {
            return Ebean.deleteAll(Association.all())
        }
    }

    @Transactional
    override fun delete(): Boolean {
        Ebean.deleteAll(childAssociations)
        parentAssociations.forEach { parentAssociation ->
            parentAssociation.childAssociations = parentAssociation.childAssociations.filter { it.id != id }
        }
        Ebean.saveAll(parentAssociations)
        return super.delete()
    }

    fun json(): String {
        return "{" +
                "  \"id\": $id," +
                "  \"abbreviation\": \"$abbreviation\"," +
                "  \"name\": \"$name\"," +
                "  \"type\": \"$type\"," +
                "  \"parentAssociationAbbreviations\": [${parentAssociations.joinToString(", ") { "\"${it.abbreviation}\"" }}]" +
                if (country != null) {
                    ",  \"country\": ${country.json()}"
                } else {
                    ""
                } +
                "}"
    }
}

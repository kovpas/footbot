package ru.kovpas.footbot.core.model

import io.ebean.Ebean
import ru.kovpas.footbot.core.model.finder.GroupFinder
import javax.persistence.*

@Entity
@Table(name = "groupz")
actual class Group actual constructor(
    @Id
    actual val id: Long,

    @Column actual val pos: Long,

    @Column(length = 150)
    actual val title: String,

    @ManyToOne(cascade = [CascadeType.REFRESH, CascadeType.MERGE, CascadeType.PERSIST])
    actual val event: Event
): BaseModel() {

    @ManyToMany(cascade = [CascadeType.REFRESH, CascadeType.MERGE, CascadeType.PERSIST])
    private var teams: List<Team> = listOf()
    actual fun teams(): List<Team> = teams

    @OneToMany(cascade = [CascadeType.ALL])
    @OrderBy("pos")
    private var games: List<Game> = listOf()
    actual fun games(): List<Game> = games

    companion object Find : GroupFinder() {
        fun deleteAll(): Int {
            return Ebean.deleteAll(Group.all())
        }
    }

}
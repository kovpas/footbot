package ru.kovpas.footbot.core.model

import io.ebean.Ebean
import ru.kovpas.footbot.core.model.finder.GoalFinder
import javax.persistence.*

@Entity
@Table(name = "goals")
actual class Goal actual constructor(
    @Id
    actual val id: Long,

    @ManyToOne(cascade = [CascadeType.REFRESH, CascadeType.MERGE, CascadeType.PERSIST])
    actual val person: Person,

    @ManyToOne(cascade = [CascadeType.REFRESH, CascadeType.MERGE, CascadeType.PERSIST])
    actual val game: Game,

    @ManyToOne(cascade = [CascadeType.REFRESH, CascadeType.MERGE, CascadeType.PERSIST])
    actual val team: Team,

    @Column actual val minute: Int,
    @Column(name = "m_offset")
    actual val offset: Int,

    @Enumerated(EnumType.STRING)
    actual val type: GoalType,

    @Column actual val ownGoal: Boolean
): BaseModel() {

    companion object Find : GoalFinder() {
        fun deleteAll(): Int {
            return Ebean.deleteAll(Goal.all())
        }
    }

}
package ru.kovpas.footbot.core.model.finder

import io.ebean.Finder
import ru.kovpas.footbot.core.model.Goal
import ru.kovpas.footbot.core.model.query.QGoal

open class GoalFinder: Finder<Long, Goal>(Goal::class.java) {
    val alias = QGoal._alias
}
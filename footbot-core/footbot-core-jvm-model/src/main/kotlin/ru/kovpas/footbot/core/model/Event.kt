package ru.kovpas.footbot.core.model

import io.ebean.Ebean
import ru.kovpas.footbot.core.model.finder.EventFinder
import javax.persistence.*

@Entity
@Table(name = "events")
actual class Event actual constructor(
    @Id
    actual val id: Long,

    @ManyToOne(cascade = [CascadeType.REFRESH, CascadeType.MERGE, CascadeType.PERSIST])
    actual val league: League,

    @ManyToOne(cascade = [CascadeType.REFRESH, CascadeType.MERGE, CascadeType.PERSIST])
    actual val season: Season,

    @Column(columnDefinition = "datetime")
    actual val startTimestamp: Long,

    @Column(columnDefinition = "datetime")
    actual val endTimestamp: Long
): BaseModel() {

    @ManyToMany(cascade = [CascadeType.REFRESH, CascadeType.MERGE, CascadeType.PERSIST])
    private var teams: List<Team> = listOf()
    actual fun teams(): List<Team> = teams

    @OneToMany(cascade = [CascadeType.ALL])
    @OrderBy("pos")
    private var rounds: List<Round> = listOf()
    actual fun rounds(): List<Round> = rounds

    @OneToMany(cascade = [CascadeType.ALL])
    @OrderBy("pos")
    private var groups: List<Group> = listOf()
    actual fun groups(): List<Group> = groups

    companion object Find : EventFinder() {
        fun deleteAll(): Int {
            return Ebean.deleteAll(Event.all())
        }
    }


}
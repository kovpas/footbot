package ru.kovpas.footbot.core.model.finder

import io.ebean.Finder
import ru.kovpas.footbot.core.model.Person
import ru.kovpas.footbot.core.model.query.QPerson

open class PersonFinder : Finder<Long, Person>(Person::class.java) {
    val alias = QPerson._alias
}
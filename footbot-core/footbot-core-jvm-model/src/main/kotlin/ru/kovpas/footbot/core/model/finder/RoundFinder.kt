package ru.kovpas.footbot.core.model.finder

import io.ebean.Finder
import ru.kovpas.footbot.core.model.Round
import ru.kovpas.footbot.core.model.query.QRound

open class RoundFinder : Finder<Long, Round>(Round::class.java) {
    val alias = QRound._alias
}
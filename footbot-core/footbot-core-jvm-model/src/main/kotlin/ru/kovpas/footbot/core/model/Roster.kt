package ru.kovpas.footbot.core.model

import io.ebean.Ebean
import ru.kovpas.footbot.core.model.finder.RosterFinder
import javax.persistence.*

@Entity
@Table(name = "games")
actual class Roster actual constructor(
    @Id
    actual val id: Long,

    @ManyToOne(cascade = [CascadeType.REFRESH, CascadeType.MERGE, CascadeType.PERSIST])
    actual val person: Person,

    @ManyToOne(cascade = [CascadeType.REFRESH, CascadeType.MERGE, CascadeType.PERSIST])
    actual val team: Team,

    @ManyToOne(cascade = [CascadeType.REFRESH, CascadeType.MERGE, CascadeType.PERSIST])
    actual val event: Event
) {

    companion object Find : RosterFinder() {
        fun deleteAll(): Int {
            return Ebean.deleteAll(Game.all())
        }
    }

}
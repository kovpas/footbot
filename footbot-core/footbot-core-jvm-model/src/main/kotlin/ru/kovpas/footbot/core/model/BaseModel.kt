package ru.kovpas.footbot.core.model

import io.ebean.Model
import io.ebean.annotation.JsonIgnore
import io.ebean.annotation.WhenCreated
import io.ebean.annotation.WhenModified
import java.time.Instant
import javax.persistence.MappedSuperclass
import javax.persistence.Version

@MappedSuperclass
open class BaseModel : Model() {

    @Version @JsonIgnore
    var version: Long = 0

    @WhenCreated @JsonIgnore
    lateinit var whenCreated: Instant

    @WhenModified @JsonIgnore
    lateinit var whenModified: Instant
}
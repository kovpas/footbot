package ru.kovpas.footbot.core.model.finder

import io.ebean.Finder
import ru.kovpas.footbot.core.model.Team
import ru.kovpas.footbot.core.model.query.QTeam

open class TeamFinder: Finder<Long, Team>(Team::class.java) {
    val alias = QTeam._alias
}
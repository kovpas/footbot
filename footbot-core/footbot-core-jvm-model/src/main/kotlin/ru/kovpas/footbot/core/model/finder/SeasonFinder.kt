package ru.kovpas.footbot.core.model.finder

import io.ebean.Finder
import ru.kovpas.footbot.core.model.Season
import ru.kovpas.footbot.core.model.query.QSeason

open class SeasonFinder : Finder<Long, Season>(Season::class.java) {
    val alias = QSeason._alias
}
package ru.kovpas.footbot.core.model

import io.ebean.Ebean
import ru.kovpas.footbot.core.model.finder.TeamFinder
import javax.persistence.*

@Entity
@Table(name = "teams")
actual class Team actual constructor(
    @Id
    actual val id: Long,

    @Column(length = 150)
    actual val title: String,

    @Enumerated(EnumType.STRING)
    actual val type: TeamType,

    @ManyToOne(cascade = [CascadeType.REFRESH, CascadeType.MERGE, CascadeType.PERSIST])
    actual val association: Association
): BaseModel() {

    @ManyToMany(mappedBy = "teams", cascade = [CascadeType.REFRESH, CascadeType.MERGE, CascadeType.PERSIST])
    private var events: List<Event> = listOf()
    actual fun events(): List<Event> = events

    @ManyToMany(mappedBy = "teams", cascade = [CascadeType.REFRESH, CascadeType.MERGE, CascadeType.PERSIST])
    @OrderBy("pos")
    private var groups: List<Group> = listOf()

    companion object Find : TeamFinder() {
        fun deleteAll(): Int {
            return Ebean.deleteAll(Team.all())
        }
    }
}
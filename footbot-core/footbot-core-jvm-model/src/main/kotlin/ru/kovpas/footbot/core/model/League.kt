package ru.kovpas.footbot.core.model

import io.ebean.Ebean
import ru.kovpas.footbot.core.model.finder.LeagueFinder
import javax.persistence.*

@Entity
@Table(name = "leagues")
actual class League actual constructor(
    @Id
    actual val id: Long,

    @Column(length = 150)
    actual val title: String,

    @Enumerated(EnumType.STRING)
    actual val type: LeagueType,

    @ManyToOne(cascade = [CascadeType.REFRESH, CascadeType.MERGE, CascadeType.PERSIST])
    actual val association: Association
) : BaseModel() {

    @OneToMany(mappedBy = "league", cascade = [CascadeType.ALL])
    private var events: List<Event> = listOf()
    actual fun events(): List<Event> = events

    companion object Find : LeagueFinder() {
        fun deleteAll(): Int {
            return Ebean.deleteAll(League.all())
        }
    }

    fun json(): String {
        return "{" +
                "  \"id\": $id," +
                "  \"title\": \"$title\"," +
                "  \"type\": \"$type\"," +
                "  \"associationId\": \"${association.abbreviation}\"" +
                "}"
    }

}
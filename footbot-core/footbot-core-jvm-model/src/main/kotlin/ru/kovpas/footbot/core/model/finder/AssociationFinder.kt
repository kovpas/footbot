package ru.kovpas.footbot.core.model.finder

import io.ebean.Finder
import ru.kovpas.footbot.core.model.Association
import ru.kovpas.footbot.core.model.query.QAssociation

open class AssociationFinder : Finder<Long, Association>(Association::class.java) {

    val alias = QAssociation._alias

    fun findByAbbreviation(abbreviation: String): Association? {
        return QAssociation()
            .abbreviation
            .equalTo(abbreviation)
            .findOne()
    }

    fun findByAbbreviations(abbreviations: List<String>): List<Association> {
        return QAssociation()
            .abbreviation
            .`in`(abbreviations)
            .findList()
    }

    fun findByName(name: String): List<Association> {
        return QAssociation()
            .name
            .icontains(name)
            .findList()
    }

    fun findByParentAssociation(parentAssociation: Association?): List<Association> {
        return QAssociation()
            .parentAssociations
            .equalTo(parentAssociation)
            .findList()
    }
}
package ru.kovpas.footbot.core.model

import io.ebean.Ebean
import ru.kovpas.footbot.core.model.finder.GameFinder
import javax.persistence.*

@Entity
@Table(name = "games")
actual class Game actual constructor(
    @Id
    actual val id: Long,

    @Column actual val pos: Long,

    @ManyToOne(cascade = [CascadeType.REFRESH, CascadeType.MERGE, CascadeType.PERSIST])
    actual val round: Round,

    @ManyToOne(cascade = [CascadeType.REFRESH, CascadeType.MERGE, CascadeType.PERSIST])
    actual val group: Group?,

    @ManyToOne(cascade = [CascadeType.REFRESH, CascadeType.MERGE, CascadeType.PERSIST])
    actual val team1: Team,

    @ManyToOne(cascade = [CascadeType.REFRESH, CascadeType.MERGE, CascadeType.PERSIST])
    actual val team2: Team,

    @Column(columnDefinition = "datetime")
    actual val startTimestamp: Long,

    @Column(columnDefinition = "datetime")
    actual val endTimestamp: Long,

    @Column(columnDefinition = "datetime")
    actual val startTimestamp2: Long?,

    @Column(columnDefinition = "datetime")
    actual val startTimestamp3: Long?,

    @Column actual val postponed: Boolean,
    @Column actual val isHome: Boolean,

    @Column actual val score1: Int?,
    @Column actual val score2: Int?,
    @Column actual val score1et: Int?,
    @Column actual val score2et: Int?,
    @Column actual val score1p: Int?,
    @Column actual val score2p: Int?,
    @Column actual val score1i: Int?,
    @Column actual val score2i: Int?,
    @Column actual val score1ii: Int?,
    @Column actual val score2ii: Int?,

    @OneToOne(cascade = [CascadeType.REFRESH, CascadeType.MERGE, CascadeType.PERSIST])
    actual val prevGame: Game?,

    @OneToOne(cascade = [CascadeType.REFRESH, CascadeType.MERGE, CascadeType.PERSIST])
    actual val nextGame: Game?
) : BaseModel() {

    @OneToMany(cascade = [CascadeType.ALL])
    private var goals: List<Goal> = listOf()
    actual fun goals(): List<Goal> = goals

    actual fun over(): Boolean = (endTimestamp * 1000) <= System.currentTimeMillis()

    @Enumerated(EnumType.STRING)
    private var winner: WinnerType? = null
    actual fun winner(): WinnerType? = winner

    @Enumerated(EnumType.STRING)
    private var winner90: WinnerType? = null
    actual fun winner90(): WinnerType? = winner90

    companion object Find : GameFinder() {
        fun deleteAll(): Int {
            return Ebean.deleteAll(Game.all())
        }
    }

    @PrePersist @PreUpdate @Suppress("Unused")
    private fun calculateWinner() {
        if (score1 == null || score2 == null) {
            winner90 = null
            winner = null
            return
        }

        winner90 = when {
            score1 > score2 -> WinnerType.HOME
            score1 < score2 -> WinnerType.AWAY
            else -> WinnerType.DRAW
        }

        winner = if (score1p != null && score2p != null) {
            when {
                score1p > score2p -> WinnerType.HOME
                score1p < score2p -> WinnerType.AWAY
                else -> WinnerType.DRAW
            }
        } else if (score1et != null && score2et != null) {
            when {
                score1et > score2et -> WinnerType.HOME
                score1et < score2et -> WinnerType.AWAY
                else -> WinnerType.DRAW
            }
        } else {
            WinnerType.DRAW
        }
    }
}
package ru.kovpas.footbot.core.model

import io.ebean.Ebean
import io.ebean.annotation.Transactional
import ru.kovpas.footbot.core.model.finder.CountryFinder
import javax.persistence.*

@Entity
@Table(name = "countries")
actual class Country actual constructor(
    @Id
    actual val id: Long,

    @Column(length = 2, unique = true)
    actual val code: String,

    @Column(length = 3, unique = true)
    actual val code3: String,

    @Column(length = 150)
    actual val name: String,

    @ManyToOne(cascade = [CascadeType.REFRESH, CascadeType.MERGE, CascadeType.PERSIST])
    actual val continent: Continent
) : BaseModel() {

    @OneToMany(mappedBy = "country", cascade = [CascadeType.REFRESH, CascadeType.MERGE, CascadeType.PERSIST])
    private var associations: List<Association> = listOf()

    actual fun associations(): List<Association> = associations

    fun json(): String {
//        return Ebean.json().toJson(this)
        return "{" +
                "  \"id\": $id," +
                "  \"code\": \"$code\"," +
                "  \"code3\": \"$code3\"," +
                "  \"name\": \"$name\"," +
                "  \"continent\": ${continent.json()}" +
                "}"
    }

    companion object Find : CountryFinder() {
        @Transactional
        fun deleteAll(): Int {
            Country.all().map { it.associations }.flatten().forEach { it.delete() }
            return Ebean.deleteAll(Country.all())
        }
    }
}
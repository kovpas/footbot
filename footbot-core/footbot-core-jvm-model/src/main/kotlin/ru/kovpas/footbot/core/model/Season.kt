package ru.kovpas.footbot.core.model

import io.ebean.Ebean
import ru.kovpas.footbot.core.model.finder.SeasonFinder
import javax.persistence.*

@Entity
@Table(name = "seasons")
actual class Season actual constructor(
    @Id
    actual val id: Long,

    @Column(length = 150)
    actual val title: String
) : BaseModel() {

    @OneToMany(mappedBy = "season", cascade = [CascadeType.ALL])
    private var events: List<Event> = listOf()
    actual fun events(): List<Event> = events

    companion object Find : SeasonFinder() {
        fun deleteAll(): Int {
            return Ebean.deleteAll(Season.all())
        }
    }

}
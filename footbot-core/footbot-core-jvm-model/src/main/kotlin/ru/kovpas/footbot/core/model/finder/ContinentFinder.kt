package ru.kovpas.footbot.core.model.finder

import io.ebean.Finder
import ru.kovpas.footbot.core.model.Continent
import ru.kovpas.footbot.core.model.query.QContinent

open class ContinentFinder: Finder<Long, Continent>(Continent::class.java) {
    val alias = QContinent._alias

    fun findByCode(code: String): Continent? {
        return QContinent()
            .code
            .equalTo(code)
            .findOne()
    }

}

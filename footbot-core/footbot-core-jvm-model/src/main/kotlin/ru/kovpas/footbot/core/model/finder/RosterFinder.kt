package ru.kovpas.footbot.core.model.finder

import io.ebean.Finder
import ru.kovpas.footbot.core.model.Roster
import ru.kovpas.footbot.core.model.query.QRoster

open class RosterFinder: Finder<Long, Roster>(Roster::class.java) {
    val alias = QRoster._alias
}
package ru.kovpas.footbot.core.model.finder

import io.ebean.Finder
import ru.kovpas.footbot.core.model.Game
import ru.kovpas.footbot.core.model.query.QGame

open class GameFinder: Finder<Long, Game>(Game::class.java) {
    val alias = QGame._alias
}
package ru.kovpas.footbot.core.model

import io.ebean.Ebean
import ru.kovpas.footbot.core.model.finder.ContinentFinder
import javax.persistence.*

@Entity
@Table(name = "continents")
actual class Continent actual constructor(
    @Id
    actual val id: Long,

    @Column(length = 2, unique = true)
    actual val code: String,

    @Column(length = 150)
    actual val name: String
) : BaseModel() {

    @OneToMany(mappedBy = "continent", cascade = [CascadeType.ALL])
    actual var countries: MutableList<Country> = mutableListOf()

    companion object Find : ContinentFinder() {
        fun deleteAll(): Int {
            Continent.all()
                .map { it.countries }.flatten()
                .map { it.associations() }.flatten()
                .forEach { it.delete() }
            return Ebean.deleteAll(Continent.all())
        }
    }

    fun json(): String {
//        return Ebean.json().toJson(this)
        return "{" +
                "  \"id\": $id," +
                "  \"code\": \"$code\"," +
                "  \"name\": \"$name\"" +
                "}"
    }
}
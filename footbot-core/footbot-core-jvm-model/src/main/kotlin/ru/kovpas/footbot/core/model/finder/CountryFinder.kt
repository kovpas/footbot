package ru.kovpas.footbot.core.model.finder

import io.ebean.Finder
import ru.kovpas.footbot.core.model.Country
import ru.kovpas.footbot.core.model.query.QCountry


open class CountryFinder : Finder<Long, Country>(Country::class.java) {
    val alias = QCountry._alias

    fun findByCode(code: String): Country? {
        return QCountry()
            .code
            .equalTo(code)
            .findOne()
    }

    fun findByCode3(code3: String): Country? {
        return QCountry()
            .code3
            .equalTo(code3)
            .findOne()
    }

}

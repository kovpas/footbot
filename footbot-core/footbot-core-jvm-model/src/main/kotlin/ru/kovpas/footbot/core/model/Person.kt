package ru.kovpas.footbot.core.model

import io.ebean.Ebean
import io.ebean.annotation.DbForeignKey
import ru.kovpas.footbot.core.model.finder.PersonFinder
import javax.persistence.*

@Entity
@Table(name = "persons")
actual class Person actual constructor(
    @Id
    actual val id: Long,

    @Column(length = 150)
    actual val name: String,

    @ManyToOne(cascade = [CascadeType.REFRESH, CascadeType.MERGE, CascadeType.PERSIST])
    @DbForeignKey(onDelete = io.ebean.annotation.ConstraintMode.CASCADE)
    actual val country: Country
) : BaseModel() {

    @OneToMany(cascade = [CascadeType.ALL])
    private var goals: List<Goal> = listOf()
    actual fun goals(): List<Goal> = goals

    companion object Find : PersonFinder() {
        fun deleteAll(): Int {
            return Ebean.deleteAll(Person.all())
        }
    }

}
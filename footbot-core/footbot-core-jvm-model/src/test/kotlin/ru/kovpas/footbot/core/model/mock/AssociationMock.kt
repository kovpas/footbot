package ru.kovpas.footbot.core.model.mock

import ru.kovpas.footbot.core.model.Association
import ru.kovpas.footbot.core.model.AssociationType

class AssociationMock(
    var id: Long = 0,
    var abbreviation: String = "abbreviation",
    var name: String = "name",
    var type: AssociationType = AssociationType.CONTINENTAL,
    var parentAssociations: List<AssociationMock> = ArrayList(),
    var country: CountryMock? = null
) {
    fun createInstance(): Association =
        Association.byId(id) ?: Association(
            id = id,
            abbreviation = abbreviation,
            name = name,
            type = type,
            parentAssociations = parentAssociations.map(AssociationMock::createInstance),
            country = country?.createInstance()
        )

    companion object {
        val FIFA = AssociationMock(
            id = 1, abbreviation = "fifa", name = "FIFA", type = AssociationType.INTERCONTINENTAL
        )
        val UEFA = AssociationMock(
            id = 2,
            abbreviation = "uefa",
            name = "UEFA",
            type = AssociationType.CONTINENTAL,
            parentAssociations = listOf(AssociationMock.FIFA)
        )
        val NED = AssociationMock(
            id = 3,
            abbreviation = "ned",
            name = "Koninklijke Nederlandse Voetbalbond",
            type = AssociationType.NATIONAL,
            parentAssociations = listOf(AssociationMock.FIFA, AssociationMock.UEFA),
            country = CountryMock.NETHERLANDS
        )
    }
}

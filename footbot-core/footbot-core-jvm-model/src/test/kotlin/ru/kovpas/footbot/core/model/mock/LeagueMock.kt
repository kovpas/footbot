package ru.kovpas.footbot.core.model.mock

import ru.kovpas.footbot.core.model.League
import ru.kovpas.footbot.core.model.LeagueType

class LeagueMock(
    var id: Long = 0,
    var title: String,
    var type: LeagueType,
    var association: AssociationMock
) {
    fun createInstance(): League = League.byId(id) ?: League(
        id = id,
        title = title,
        type = type,
        association = association.createInstance()
    )

    companion object {
        val EREDIVISIE =
            LeagueMock(id = 1, title = "Eredivisie", type = LeagueType.CLUB, association = AssociationMock.NED)
        val EERSTEDIVISIE =
            LeagueMock(id = 2, title = "Eerste Divisie", type = LeagueType.CLUB, association = AssociationMock.NED)
    }
}
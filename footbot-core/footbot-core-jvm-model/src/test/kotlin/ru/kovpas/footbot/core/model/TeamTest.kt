package ru.kovpas.footbot.core.model

import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import ru.kovpas.footbot.core.model.mock.AssociationMock
import ru.kovpas.footbot.core.model.mock.TeamMock
import kotlin.test.assertEquals

class TeamTest {

    @BeforeEach
    fun setUp() {
        // have to save FIFA first, so UEFA and NED wouldn't compete for who saves it first
        AssociationMock.FIFA.createInstance().save()
    }

    @AfterEach
    fun tearDown() {
        Continent.deleteAll()
    }

    @Test
    fun deleteAll() {
        TeamMock.AJAX.createInstance().save()

        assertEquals(1, Team.all().count())
        Team.deleteAll()
        assertEquals(0, Team.all().count())
    }

}
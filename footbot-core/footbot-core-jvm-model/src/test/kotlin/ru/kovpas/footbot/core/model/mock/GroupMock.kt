package ru.kovpas.footbot.core.model.mock

import ru.kovpas.footbot.core.model.Group

class GroupMock(
    var id: Long = 1,
    var pos: Long = 0,
    var title: String = "title",
    var event: EventMock = EventMock.EREDIVISIE_SEASON_2018_19
) {
    fun createInstance(): Group = Group.byId(id) ?: Group(
        id = id,
        pos = pos,
        title = title,
        event = event.createInstance()
    )

    companion object {
        val EREDIVISIE_SEASON_2018_19 = GroupMock(
            id = 1,
            pos = 1,
            title = "Eredivisie",
            event = EventMock.EREDIVISIE_SEASON_2018_19
        )

        val EREDIVISIE_SEASON_2017_18 = GroupMock(
            id = 2,
            pos = 2,
            title = "Eredivisie",
            event = EventMock.EREDIVISIE_SEASON_2017_18
        )
    }
}
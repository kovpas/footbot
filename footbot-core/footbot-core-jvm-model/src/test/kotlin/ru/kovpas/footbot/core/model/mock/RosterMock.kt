package ru.kovpas.footbot.core.model.mock

import ru.kovpas.footbot.core.model.Roster

class RosterMock(
    var id: Long = 0,
    var person: PersonMock = PersonMock.ROBBEN,
    var team: TeamMock = TeamMock.AJAX,
    var event: EventMock = EventMock.EREDIVISIE_SEASON_2018_19
) {
    fun createInstance(): Roster = Roster.byId(id) ?: Roster(
        id = id,
        person = person.createInstance(),
        team = team.createInstance(),
        event = event.createInstance()
    )

    companion object {
        val ROBBEN_AJAX = RosterMock(
            id = 1,
            person = PersonMock.ROBBEN,
            team = TeamMock.AJAX,
            event = EventMock.EREDIVISIE_SEASON_2018_19
        )

        val VAN_PERSIE_PSV = RosterMock(
            id = 2,
            person = PersonMock.VAN_PERSIE,
            team = TeamMock.PSV,
            event = EventMock.EREDIVISIE_SEASON_2017_18
        )
    }
}
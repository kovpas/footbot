package ru.kovpas.footbot.core.model.mock

import ru.kovpas.footbot.core.model.Game

class GameMock(
    var id: Long = 1,
    var pos: Long = 1,
    var round: RoundMock = RoundMock.FIRST_ROUND,
    var group: GroupMock? = null,
    var team1: TeamMock = TeamMock.AJAX,
    var team2: TeamMock = TeamMock.PSV,
    var startTimestamp: Long = 1533924000,
    var endTimestamp: Long = 1533934000,
    var startTimestamp2: Long? = null,
    var startTimestamp3: Long? = null,
    var postponed: Boolean = false,
    var isHome: Boolean = true,
    var score1: Int = 1,
    var score2: Int = 2,
    var score1et: Int = 0,
    var score2et: Int = 0,
    var score1p: Int = 0,
    var score2p: Int = 0,
    var score1i: Int = 0,
    var score2i: Int = 0,
    var score1ii: Int = 0,
    var score2ii: Int = 0
) {
    fun createInstance(): Game = Game.byId(id) ?: Game(
        id = id,
        pos = pos,
        round = round.createInstance(),
        group = group?.createInstance(),
        team1 = team1.createInstance(),
        team2 = team2.createInstance(),
        startTimestamp = startTimestamp,
        endTimestamp = endTimestamp,
        startTimestamp2 = startTimestamp2,
        startTimestamp3 = startTimestamp3,
        postponed = postponed,
        isHome = isHome,
        score1 = score1,
        score2 = score2,
        score1et = score1et,
        score2et = score2et,
        score1p = score1p,
        score2p = score2p,
        score1i = score1i,
        score2i = score2i,
        score1ii = score1ii,
        score2ii = score2ii
    )

    companion object {
        val AJAX_PSV_2018_19 = GameMock(
            id = 1,
            pos = 1,
            round = RoundMock.FIRST_ROUND,
            group = GroupMock.EREDIVISIE_SEASON_2018_19,
            team1 = TeamMock.AJAX,
            team2 = TeamMock.PSV,
            startTimestamp = 1533924000,
            endTimestamp = 1533944000,
            postponed = false,
            isHome = true,
            score1 = 2,
            score2 = 1,
            score1et = 0,
            score2et = 0,
            score1p = 0,
            score2p = 0,
            score1i = 0,
            score2i = 0,
            score1ii = 0,
            score2ii = 0
        )

        val PSV_AJAX_2018_19 = GameMock(
            id = 1,
            pos = 1,
            round = RoundMock.SECOND_ROUND,
            group = GroupMock.EREDIVISIE_SEASON_2018_19,
            team1 = TeamMock.PSV,
            team2 = TeamMock.AJAX,
            startTimestamp = 1534615200,
            endTimestamp = 1534665200,
            postponed = false,
            isHome = true,
            score1 = 1,
            score2 = 1,
            score1et = 0,
            score2et = 0,
            score1p = 0,
            score2p = 0,
            score1i = 0,
            score2i = 0,
            score1ii = 0,
            score2ii = 0
        )
    }
}
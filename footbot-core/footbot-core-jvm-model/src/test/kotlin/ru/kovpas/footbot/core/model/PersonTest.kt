package ru.kovpas.footbot.core.model

import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import ru.kovpas.footbot.core.model.mock.CountryMock
import ru.kovpas.footbot.core.model.mock.PersonMock
import ru.kovpas.footbot.core.model.mock.SeasonMock
import kotlin.test.assertEquals

internal class PersonTest {

    @BeforeEach
    fun setUp() {
        PersonMock.ROBBEN.createInstance().save()
    }

    @AfterEach
    fun tearDown() {
        Continent.deleteAll()
    }

    @Test
    fun deleteAll() {
        assertEquals(1, Country.all().count())
        assertEquals(1, Person.all().count())

        Person.deleteAll()

        assertEquals(1, Country.all().count())
        assertEquals(0, Person.all().count())
    }

    @Test
    fun cascadeDeleteCountry() {
        val ned = Country.byId(CountryMock.NETHERLANDS.id) ?: throw Exception("no netherlands :)")
        assertEquals(1, Person.all().count())

        ned.delete()

        assertEquals(0, Person.all().count())
    }

//    @Test
//    fun json() {
//        val netherlands = Country.findByCode("nl")
//
//        val expectedNlJson = "{" +
//                "  \"id\": $netherlandsId," +
//                "  \"code\": \"nl\"," +
//                "  \"code3\": \"NED\"," +
//                "  \"name\": \"Netherlands\"," +
//                "  \"continent\": {" +
//                "  \"id\": $europeId," +
//                "  \"code\": \"eu\"," +
//                "  \"name\": \"Europe\"" +
//                "}" +
//                "}"
//
//        assertEquals(netherlands?.json(), expectedNlJson)
//    }

}
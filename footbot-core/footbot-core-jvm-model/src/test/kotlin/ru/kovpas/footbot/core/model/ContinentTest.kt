package ru.kovpas.footbot.core.model

import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import ru.kovpas.footbot.core.model.mock.ContinentMock
import kotlin.test.assertEquals

internal class ContinentTest {

    @BeforeEach
    fun setUp() {
        ContinentMock.EUROPE.createInstance().save()
    }

    @AfterEach
    fun tearDown() {
        Continent.deleteAll()
    }

    @Test
    fun findByCode() {
        val continent = Continent.findByCode(ContinentMock.EUROPE.code) ?: throw Exception("no europe :)")

        assertEquals(ContinentMock.EUROPE.id, continent.id)
    }

    @Test
    fun deleteAll() {
        val europe = Continent.findByCode("eu") ?: throw java.lang.Exception("no europe :)")
        Country(id = 100, code = "nl", code3 = "NED", name = "Netherlands", continent = europe).save()

        assertEquals(1, Country.all().count())

        Continent.deleteAll()

        assertEquals(0, Continent.all().count())
        assertEquals(0, Country.all().count())
    }

    @Test
    fun countriesOfContinent() {
        val europe = Continent.findByCode("eu") ?: throw java.lang.Exception("no europe :)")
        Country(code = "nl", code3 = "NED", name = "Netherlands", continent = europe).save()

        assertEquals(1, europe.countries.count { it.code == "nl" })
    }

    @Test
    fun json() {
        val europe = Continent.findByCode("eu") ?: throw java.lang.Exception("no europe :)")

        val expectedEuJson = "{" +
                "  \"id\": ${ContinentMock.EUROPE.id}," +
                "  \"code\": \"${ContinentMock.EUROPE.code}\"," +
                "  \"name\": \"${ContinentMock.EUROPE.name}\"" +
                "}"

        assertEquals(expectedEuJson, europe.json())
    }

}
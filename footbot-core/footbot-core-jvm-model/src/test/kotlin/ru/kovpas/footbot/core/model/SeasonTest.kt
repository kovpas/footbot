package ru.kovpas.footbot.core.model

import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import ru.kovpas.footbot.core.model.mock.SeasonMock
import kotlin.test.assertEquals

internal class SeasonTest {

    @BeforeEach
    fun setUp() {
        SeasonMock.SEASON_2018_19.createInstance().save()
    }

    @AfterEach
    fun tearDown() {
        Season.deleteAll()
    }

    @Test
    fun deleteAll() {
        assertEquals(1, Season.all().count())

        Season.deleteAll()

        assertEquals(0, Season.all().count())
    }

//    @Test
//    fun json() {
//        val netherlands = Country.findByCode("nl")
//
//        val expectedNlJson = "{" +
//                "  \"id\": $netherlandsId," +
//                "  \"code\": \"nl\"," +
//                "  \"code3\": \"NED\"," +
//                "  \"name\": \"Netherlands\"," +
//                "  \"continent\": {" +
//                "  \"id\": $europeId," +
//                "  \"code\": \"eu\"," +
//                "  \"name\": \"Europe\"" +
//                "}" +
//                "}"
//
//        assertEquals(netherlands?.json(), expectedNlJson)
//    }

}
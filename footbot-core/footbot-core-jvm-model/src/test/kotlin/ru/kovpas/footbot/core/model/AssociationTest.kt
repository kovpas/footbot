package ru.kovpas.footbot.core.model

import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import ru.kovpas.footbot.core.model.mock.AssociationMock
import ru.kovpas.footbot.core.model.mock.CountryMock
import kotlin.test.assertEquals
import kotlin.test.assertNotNull
import kotlin.test.assertNull

class AssociationTest {

    @BeforeEach
    fun setUp() {
        AssociationMock.FIFA.createInstance().save()
    }

    @AfterEach
    fun tearDown() {
        Association.deleteAll()
        Continent.deleteAll()
    }

    @Test
    fun childAssociations() {
        val fifa = AssociationMock.FIFA.createInstance()
        val uefa = AssociationMock.UEFA.createInstance(); uefa.save(); uefa.refresh()
        val ned = AssociationMock.NED.createInstance(); ned.save()

        val fifaChildAssociations = fifa.childAssociations()
        val uefaChildAssociations = uefa.childAssociations()

        assertEquals(2, fifaChildAssociations.count())
        assert(fifaChildAssociations.contains(uefa))
        assert(fifaChildAssociations.contains(ned))

        assertEquals(1, uefaChildAssociations.count())
        assert(uefaChildAssociations.contains(ned))
    }

    @Test
    fun findByAbbreviation() {
        val fifa = AssociationMock.FIFA.createInstance()
        val uefa = AssociationMock.UEFA.createInstance(); uefa.save()

        val foundConmebol = Association.findByAbbreviation("conmebol")
        assertNull(foundConmebol)

        val foundUefa = Association.findByAbbreviation(AssociationMock.UEFA.abbreviation)
        assertNotNull(foundUefa)
        assertEquals(AssociationMock.UEFA.id, foundUefa.id)
        assert(foundUefa.parentAssociations.contains(fifa))
    }

    @Test
    fun findByAbbreviations() {
        val fifa = Association.findByAbbreviation(AssociationMock.FIFA.abbreviation) ?: throw Exception("No fifa")
        val uefa = AssociationMock.UEFA.createInstance(); uefa.save()

        val associations = Association.findByAbbreviations(
            listOf(
                AssociationMock.FIFA.abbreviation,
                AssociationMock.UEFA.abbreviation
            )
        )
        assertEquals(2, associations.count())
        assert(associations.containsAll(listOf(fifa, uefa)))

        val emptyAssociations = Association.findByAbbreviations(listOf())
        assertEquals(0, emptyAssociations.count())
    }

    @Test
    fun findByName() {
        val fifa = Association.findByAbbreviation(AssociationMock.FIFA.abbreviation) ?: throw Exception("No fifa")
        val uefa = AssociationMock.UEFA.createInstance(); uefa.save()
        val ned = AssociationMock.NED.createInstance(); ned.save()

        val uefaAndFifa = Association.findByName("FA")

        assertEquals(2, uefaAndFifa.count())
        assert(uefaAndFifa.containsAll(listOf(uefa, fifa)))
    }

    @Test
    fun findByParentAssociation() {
        val fifa = Association.findByAbbreviation(AssociationMock.FIFA.abbreviation) ?: throw Exception("No fifa")
        val uefa = AssociationMock.UEFA.createInstance(); uefa.save()
        val ned = AssociationMock.NED.createInstance(); ned.save()

        val childAssociations = Association.findByParentAssociation(fifa)

        assertEquals(2, childAssociations.count())
        assert(childAssociations.containsAll(listOf(ned, uefa)))
    }

    @Test
    fun deleteAll() {
        Association.deleteAll()

        assertEquals(0, Association.all().count())
    }

    @Test
    fun deleteCountryDeletesAssociations() {
        AssociationMock.NED.createInstance().save()
        assertEquals(3, Association.all().count())

        Country.deleteAll()

        assertEquals(2, Association.all().count())
    }

    @Test
    fun deleteContinentDeletesAssociations() {
        AssociationMock.NED.createInstance().save()
        assertEquals(3, Association.all().count())

        Continent.deleteAll()

        assertEquals(2, Association.all().count())
    }

    @Test
    fun deleteOne() {
        val uefa = AssociationMock.UEFA.createInstance(); uefa.save()
        val ned = AssociationMock.NED.createInstance(); ned.save()

        assertEquals(3, Association.all().count())

        uefa.delete()

        assertEquals(2, Association.all().count())

        val newFifa = Association.findByAbbreviation(AssociationMock.FIFA.abbreviation)
        val newNed = Association.findByAbbreviation(AssociationMock.NED.abbreviation)

        assertNotNull(newFifa)
        assertNotNull(newNed)
        assertEquals(1, newFifa.childAssociations().count())
    }

    @Test
    fun json() {
        val fifa = Association.findByAbbreviation(AssociationMock.FIFA.abbreviation) ?: throw Exception("No fifa")
        val uefa = AssociationMock.UEFA.createInstance(); uefa.save()
        val ned = AssociationMock.NED.createInstance(); ned.save()

        val expectedNedJson = "{" +
                "  \"id\": ${AssociationMock.NED.id}," +
                "  \"abbreviation\": \"${AssociationMock.NED.abbreviation}\"," +
                "  \"name\": \"${AssociationMock.NED.name}\"," +
                "  \"type\": \"${AssociationMock.NED.type}\"," +
                "  \"parentAssociationAbbreviations\": [\"${AssociationMock.FIFA.abbreviation}\", \"${AssociationMock.UEFA.abbreviation}\"]," +
                "  \"country\": {" +
                "  \"id\": ${AssociationMock.NED.country?.id}," +
                "  \"code\": \"${AssociationMock.NED.country?.code}\"," +
                "  \"code3\": \"${AssociationMock.NED.country?.code3}\"," +
                "  \"name\": \"${AssociationMock.NED.country?.name}\"," +
                "  \"continent\": {" +
                "  \"id\": ${AssociationMock.NED.country?.continent?.id}," +
                "  \"code\": \"${AssociationMock.NED.country?.continent?.code}\"," +
                "  \"name\": \"${AssociationMock.NED.country?.continent?.name}\"" +
                "}" +
                "}" +
                "}"

        assertEquals(expectedNedJson, ned.json())

        val expectedFifaJson = "{" +
                "  \"id\": ${AssociationMock.FIFA.id}," +
                "  \"abbreviation\": \"${AssociationMock.FIFA.abbreviation}\"," +
                "  \"name\": \"${AssociationMock.FIFA.name}\"," +
                "  \"type\": \"${AssociationMock.FIFA.type}\"," +
                "  \"parentAssociationAbbreviations\": []" +
                "}"

        assertEquals(expectedFifaJson, fifa.json())
    }

}
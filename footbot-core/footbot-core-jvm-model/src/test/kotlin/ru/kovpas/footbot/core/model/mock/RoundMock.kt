package ru.kovpas.footbot.core.model.mock

import ru.kovpas.footbot.core.model.Round
import ru.kovpas.footbot.core.model.RoundType

class RoundMock(
    var id: Long = 1,
    var pos: Long = 1,
    var title: String = "title",
    var type: RoundType = RoundType.GROUP,
    var event: EventMock = EventMock.EREDIVISIE_SEASON_2018_19,
    var startTimestamp: Long = 1533924000,
    var endTimestamp: Long = 1534176900
) {
    fun createInstance(): Round = Round.byId(id) ?: Round(
        id = id,
        pos = pos,
        title = title,
        type = type,
        event = event.createInstance(),
        startTimestamp = startTimestamp,
        endTimestamp = endTimestamp
    )

    companion object {
        val FIRST_ROUND = RoundMock(
            id = 1,
            pos = 1,
            title = "Speelronde 1",
            type = RoundType.GROUP,
            event = EventMock.EREDIVISIE_SEASON_2018_19,
            startTimestamp = 1533924000,
            endTimestamp = 1534176900
        )
        val SECOND_ROUND = RoundMock(
            id = 2,
            pos = 2,
            title = "Speelronde 2",
            type = RoundType.GROUP,
            event = EventMock.EREDIVISIE_SEASON_2018_19,
            startTimestamp = 1534615200,
            endTimestamp = 1534781700
        )
    }
}
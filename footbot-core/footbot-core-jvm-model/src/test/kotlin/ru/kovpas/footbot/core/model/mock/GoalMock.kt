package ru.kovpas.footbot.core.model.mock

import ru.kovpas.footbot.core.model.Goal
import ru.kovpas.footbot.core.model.GoalType

class GoalMock(
    var id: Long = 1,
    var person: PersonMock = PersonMock.VAN_PERSIE,
    var game: GameMock = GameMock.AJAX_PSV_2018_19,
    var team: TeamMock = TeamMock.AJAX,
    var minute: Int = 45,
    var offset: Int = 2,
    var type: GoalType = GoalType.GAME,
    var ownGoal: Boolean = false
) {
    fun createInstance(): Goal = Goal.byId(id) ?: Goal(
        id = id,
        person = person.createInstance(),
        game = game.createInstance(),
        team = team.createInstance(),
        minute = minute,
        offset = offset,
        type = type,
        ownGoal = ownGoal
    )

    companion object {
        val VAN_PERSIE_AJAX_PSV = GoalMock(
            id = 1,
            person = PersonMock.VAN_PERSIE,
            game = GameMock.AJAX_PSV_2018_19,
            team = TeamMock.AJAX,
            minute = 45,
            offset = 2,
            type = GoalType.GAME,
            ownGoal = false
        )

        val ROBBEN_AJAX_PSV = GoalMock(
            id = 2,
            person = PersonMock.ROBBEN,
            game = GameMock.AJAX_PSV_2018_19,
            team = TeamMock.PSV,
            minute = 75,
            offset = 0,
            type = GoalType.PENALTY,
            ownGoal = false
        )
    }
}
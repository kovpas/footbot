package ru.kovpas.footbot.core.model.mock

import ru.kovpas.footbot.core.model.Continent

class ContinentMock(
    var id: Long = 0,
    var code: String = "code",
    var name: String = "name"
) {
    fun createInstance(): Continent = Continent.byId(id) ?: Continent(
        id = id,
        code = code,
        name = name
    )

    companion object {
        val EUROPE = ContinentMock(
            id = 1,
            code = "eu",
            name = "Europe"
        )
    }

}
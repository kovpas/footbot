package ru.kovpas.footbot.core.model

import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import ru.kovpas.footbot.core.model.mock.AssociationMock
import ru.kovpas.footbot.core.model.mock.LeagueMock
import kotlin.test.assertEquals

internal class LeagueTest {

    @BeforeEach
    fun setUp() {
        // have to save FIFA first, so UEFA and NED wouldn't compete for who saves it first
        AssociationMock.FIFA.createInstance().save()
        LeagueMock.EREDIVISIE.createInstance().save()
    }

    @AfterEach
    fun tearDown() {
        Continent.deleteAll()
    }

    @Test
    fun deleteAll() {
        val associationsCount = Association.all().count()

        assertEquals(1, League.all().count())

        League.deleteAll()

        assertEquals(0, League.all().count())
        assertEquals(associationsCount, Association.all().count())
    }

    @Test
    fun associationLeagues() {
        val ned = Association.findByAbbreviation(AssociationMock.NED.abbreviation) ?: throw Exception("no ned")
        LeagueMock.EERSTEDIVISIE.createInstance().save()

        assertEquals(2, League.all().count())
        assertEquals(2, ned.leagues().count())
    }

//    @Test
//    fun json() {
//        val netherlands = Country.findByCode("nl")
//
//        val expectedNlJson = "{" +
//                "  \"id\": $netherlandsId," +
//                "  \"code\": \"nl\"," +
//                "  \"code3\": \"NED\"," +
//                "  \"name\": \"Netherlands\"," +
//                "  \"continent\": {" +
//                "  \"id\": $europeId," +
//                "  \"code\": \"eu\"," +
//                "  \"name\": \"Europe\"" +
//                "}" +
//                "}"
//
//        assertEquals(netherlands?.json(), expectedNlJson)
//    }

}
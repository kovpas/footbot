package ru.kovpas.footbot.core.model.mock

import ru.kovpas.footbot.core.model.Team
import ru.kovpas.footbot.core.model.TeamType

class TeamMock(
    var id: Long = 0,
    var title: String = "title",
    var type: TeamType = TeamType.CLUB,
    var association: AssociationMock = AssociationMock.NED
) {
    fun createInstance(): Team = Team.byId(id) ?: Team(
        id = id,
        title = title,
        type = type,
        association = association.createInstance()
    )

    companion object {
        val AJAX = TeamMock(
            id = 1,
            title = "AFC Ajax",
            type = TeamType.CLUB,
            association = AssociationMock.NED
        )

        val PSV = TeamMock(
            id = 2,
            title = "PSV",
            type = TeamType.CLUB,
            association = AssociationMock.NED
        )
    }
}
package ru.kovpas.footbot.core.model.mock

import ru.kovpas.footbot.core.model.Person

class PersonMock(
    var id: Long = 1,
    var name: String = "name",
    var country: CountryMock = CountryMock.NETHERLANDS
) {
    fun createInstance(): Person = Person.byId(id) ?: Person(id = id, name = name, country = country.createInstance())

    companion object {
        val ROBBEN = PersonMock(id = 1, name = "Arjen Robben", country = CountryMock.NETHERLANDS)
        val VAN_PERSIE = PersonMock(id = 2, name = "Robin van Persie", country = CountryMock.NETHERLANDS)
    }
}
package ru.kovpas.footbot.core.model.mock

import ru.kovpas.footbot.core.model.Event

class EventMock(
    var id: Long = 1,
    var league: LeagueMock = LeagueMock.EREDIVISIE,
    var season: SeasonMock = SeasonMock.SEASON_2018_19,
    var startTimestamp: Long = 1533924000,
    var endTimestamp: Long = 1557671400
) {
    fun createInstance(): Event = Event.byId(id) ?: Event(
        id = id,
        league = league.createInstance(),
        season = season.createInstance(),
        startTimestamp = startTimestamp,
        endTimestamp = endTimestamp
    )

    companion object {
        val EREDIVISIE_SEASON_2018_19 = EventMock(
            id = 1,
            league = LeagueMock.EREDIVISIE,
            season = SeasonMock.SEASON_2018_19,
            startTimestamp = 1533924000,
            endTimestamp = 1557671400
        )

        val EREDIVISIE_SEASON_2017_18 = EventMock(
            id = 1,
            league = LeagueMock.EREDIVISIE,
            season = SeasonMock.SEASON_2017_18,
            startTimestamp = 1502470800,
            endTimestamp = 1525615200
        )
    }
}
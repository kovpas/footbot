package ru.kovpas.footbot.core.model

import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import ru.kovpas.footbot.core.model.mock.ContinentMock
import ru.kovpas.footbot.core.model.mock.CountryMock
import kotlin.test.assertEquals
import kotlin.test.assertNotNull

internal class CountryTest {

    @BeforeEach
    fun setUp() {
        CountryMock.NETHERLANDS.createInstance().save()
    }

    @AfterEach
    fun tearDown() {
        Continent.deleteAll()
    }

    @Test
    fun findByCode() {
        assertEquals(1, Country.all().count())

        val netherlands = Country.findByCode(CountryMock.NETHERLANDS.code)

        assertNotNull(netherlands)
        assertEquals(CountryMock.NETHERLANDS.id, netherlands.id)
    }

    @Test
    fun findByCode3() {
        assertEquals(1, Country.all().count())

        val netherlands = Country.findByCode3(CountryMock.NETHERLANDS.code3)

        assertNotNull(netherlands)
        assertEquals(CountryMock.NETHERLANDS.id, netherlands.id)
    }

    @Test
    fun deleteAll() {
        val continentCount = Continent.all().count()

        assertEquals(1, Country.all().count())

        Country.deleteAll()

        assertEquals(0, Country.all().count())
        assertEquals(continentCount, Continent.all().count())

        Continent.deleteAll()

        assertEquals(0, Continent.all().count())
    }

    @Test
    fun json() {
        val netherlands = CountryMock.NETHERLANDS.createInstance()

        val expectedNlJson = "{" +
                "  \"id\": ${CountryMock.NETHERLANDS.id}," +
                "  \"code\": \"${CountryMock.NETHERLANDS.code}\"," +
                "  \"code3\": \"${CountryMock.NETHERLANDS.code3}\"," +
                "  \"name\": \"${CountryMock.NETHERLANDS.name}\"," +
                "  \"continent\": {" +
                "  \"id\": ${ContinentMock.EUROPE.id}," +
                "  \"code\": \"${ContinentMock.EUROPE.code}\"," +
                "  \"name\": \"${ContinentMock.EUROPE.name}\"" +
                "}" +
                "}"

        assertEquals(expectedNlJson, netherlands.json())
    }

}
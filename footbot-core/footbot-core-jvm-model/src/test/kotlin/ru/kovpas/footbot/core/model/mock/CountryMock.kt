package ru.kovpas.footbot.core.model.mock

import ru.kovpas.footbot.core.model.Country

class CountryMock(
    var id: Long = 0,
    var code: String = "code",
    var code3: String = "code3",
    var name: String = "name",
    var continent: ContinentMock
) {
    fun createInstance(): Country = Country.byId(id) ?: Country(
        id = id,
        code = code,
        code3 = code3,
        name = name,
        continent = continent.createInstance()
    )

    companion object {
        val NETHERLANDS = CountryMock(
            id = 1,
            code = "nl",
            code3 = "NED",
            name = "Netherlands",
            continent = ContinentMock.EUROPE
        )
    }
}
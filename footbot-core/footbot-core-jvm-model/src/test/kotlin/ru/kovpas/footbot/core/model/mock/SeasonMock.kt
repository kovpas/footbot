package ru.kovpas.footbot.core.model.mock

import ru.kovpas.footbot.core.model.Season

class SeasonMock(var id: Long, var title: String) {
    fun createInstance(): Season = Season.byId(id) ?: Season(id = id, title = title)

    companion object {
        val SEASON_2017_18 = SeasonMock(id = 1, title = "Seizoen 2017/18")
        val SEASON_2018_19 = SeasonMock(id = 2, title = "Seizoen 2018/19")
    }
}
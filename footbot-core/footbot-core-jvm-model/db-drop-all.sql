alter table associations drop constraint if exists fk_associations_country_id;
drop index if exists ix_associations_country_id;

alter table associations_associations drop constraint if exists fk_associations_associations_associations_1;
drop index if exists ix_associations_associations_associations_1;

alter table associations_associations drop constraint if exists fk_associations_associations_associations_2;
drop index if exists ix_associations_associations_associations_2;

alter table countries drop constraint if exists fk_countries_continent_id;
drop index if exists ix_countries_continent_id;

alter table events drop constraint if exists fk_events_league_id;
drop index if exists ix_events_league_id;

alter table events drop constraint if exists fk_events_season_id;
drop index if exists ix_events_season_id;

alter table events_teams drop constraint if exists fk_events_teams_events;
drop index if exists ix_events_teams_events;

alter table events_teams drop constraint if exists fk_events_teams_teams;
drop index if exists ix_events_teams_teams;

alter table games drop constraint if exists fk_games_person_id;
drop index if exists ix_games_person_id;

alter table games drop constraint if exists fk_games_team_id;
drop index if exists ix_games_team_id;

alter table games drop constraint if exists fk_games_event_id;
drop index if exists ix_games_event_id;

alter table goals drop constraint if exists fk_goals_person_id;
drop index if exists ix_goals_person_id;

alter table goals drop constraint if exists fk_goals_game_id;
drop index if exists ix_goals_game_id;

alter table goals drop constraint if exists fk_goals_team_id;
drop index if exists ix_goals_team_id;

alter table groupz drop constraint if exists fk_groupz_event_id;
drop index if exists ix_groupz_event_id;

alter table groupz_teams drop constraint if exists fk_groupz_teams_groupz;
drop index if exists ix_groupz_teams_groupz;

alter table groupz_teams drop constraint if exists fk_groupz_teams_teams;
drop index if exists ix_groupz_teams_teams;

alter table leagues drop constraint if exists fk_leagues_association_id;
drop index if exists ix_leagues_association_id;

alter table persons drop constraint if exists fk_persons_country_id;
drop index if exists ix_persons_country_id;

alter table rounds drop constraint if exists fk_rounds_event_id;
drop index if exists ix_rounds_event_id;

alter table teams drop constraint if exists fk_teams_association_id;
drop index if exists ix_teams_association_id;

drop table if exists associations;

drop table if exists associations_associations;

drop table if exists continents;

drop table if exists countries;

drop table if exists events;

drop table if exists events_teams;

drop table if exists games;

drop table if exists goals;

drop table if exists groupz;

drop table if exists groupz_teams;

drop table if exists leagues;

drop table if exists persons;

drop table if exists rounds;

drop table if exists seasons;

drop table if exists teams;


import org.gradle.api.internal.HasConvention
import org.jetbrains.kotlin.gradle.plugin.KotlinSourceSet
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

description = "Footbot model"

group = "ru.kovpas.footbot.core.model"
version = "0.1.0"

buildscript {
    val ebean_gradle_plugin_version: String by project
    dependencies {
        classpath("io.ebean", "ebean-gradle-plugin", ebean_gradle_plugin_version)
    }
}

plugins {
//    kotlin("jvm")
//    kotlin("kapt")
    id("kotlin-platform-jvm")
    id("io.ebean")
    idea
}

val ebean_version: String by project
val ebean_querybean_version: String by project
val ebean_kotlin_querybean_generator_version: String by project
val junit_version: String by project
val kotlin_version: String by project
val h2_version: String by project
val mysql_connector_version: String by project
val slf4j_version: String by project
val logback_version: String by project

dependencies {
    compile(kotlin("stdlib"))

    compile("mysql", "mysql-connector-java", mysql_connector_version)
    compile("org.slf4j", "slf4j-api", slf4j_version)
    compile("org.avaje.composite", "logback", logback_version)
    compile("io.ebean", "ebean", ebean_version)
    compile("io.ebean", "ebean-querybean", ebean_querybean_version)
//    compile("com.fasterxml.jackson.core", "jackson-annotations", "2.9.7")
    kapt("io.ebean", "kotlin-querybean-generator", ebean_kotlin_querybean_generator_version)

    expectedBy(project(":footbot-core:footbot-core-model"))

    testCompile("com.h2database", "h2", h2_version)
    testCompile("org.junit.jupiter", "junit-jupiter-api", junit_version)
    testCompile("org.junit.jupiter", "junit-jupiter-params", junit_version)
    testRuntime("org.junit.jupiter", "junit-jupiter-engine", junit_version)
    testCompile("org.jetbrains.kotlin", "kotlin-test-junit", kotlin_version)
}

val kaptGenerated = file("$buildDir/generated/source/kaptKotlin/main")
idea {
    module {
        sourceDirs.add(kaptGenerated)
        generatedSourceDirs.add(kaptGenerated)
    }
}

val SourceSet.kotlin: SourceDirectorySet
    get() = (this as HasConvention).convention.getPlugin(KotlinSourceSet::class.java).kotlin

sourceSets["main"].kotlin.srcDir(kaptGenerated)

ebean {
    debugLevel = 2
    queryBeans = true
    kotlin = true
}


// Workaround for "Task with name 'compileGeneratedKotlin' not found" problem
sourceSets.create("generated").java.srcDirs.add(kaptGenerated)
sourceSets.remove(sourceSets.getByName("generated"))
tasks.remove(tasks.findByName("processGeneratedResources"))
tasks.remove(tasks.findByName("compileGeneratedJava"))
tasks.remove(tasks.findByName("generatedClasses"))

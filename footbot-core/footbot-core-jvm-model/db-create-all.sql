create table associations (
  id                            bigint auto_increment not null,
  abbreviation                  varchar(20) not null,
  name                          varchar(150) not null,
  type                          varchar(16) not null,
  country_id                    bigint,
  version                       bigint not null,
  when_created                  timestamp not null,
  when_modified                 timestamp not null,
  constraint ck_associations_type check ( type in ('NATIONAL','CONTINENTAL','INTERCONTINENTAL')),
  constraint uq_associations_abbreviation unique (abbreviation),
  constraint pk_associations primary key (id)
);

create table associations_associations (
  parent_association_id         bigint not null,
  child_association_id          bigint not null,
  constraint pk_associations_associations primary key (parent_association_id,child_association_id)
);

create table continents (
  id                            bigint auto_increment not null,
  code                          varchar(2) not null,
  name                          varchar(150) not null,
  version                       bigint not null,
  when_created                  timestamp not null,
  when_modified                 timestamp not null,
  constraint uq_continents_code unique (code),
  constraint pk_continents primary key (id)
);

create table countries (
  id                            bigint auto_increment not null,
  code                          varchar(2) not null,
  code3                         varchar(3) not null,
  name                          varchar(150) not null,
  continent_id                  bigint not null,
  version                       bigint not null,
  when_created                  timestamp not null,
  when_modified                 timestamp not null,
  constraint uq_countries_code unique (code),
  constraint uq_countries_code3 unique (code3),
  constraint pk_countries primary key (id)
);

create table events (
  id                            bigint auto_increment not null,
  league_id                     bigint not null,
  season_id                     bigint not null,
  start_timestamp               datetime not null,
  end_timestamp                 datetime not null,
  version                       bigint not null,
  when_created                  timestamp not null,
  when_modified                 timestamp not null,
  constraint pk_events primary key (id)
);

create table events_teams (
  events_id                     bigint not null,
  teams_id                      bigint not null,
  constraint pk_events_teams primary key (events_id,teams_id)
);

create table games (
  id                            bigint auto_increment not null,
  person_id                     bigint not null,
  team_id                       bigint not null,
  event_id                      bigint not null,
  constraint pk_games primary key (id)
);

create table goals (
  id                            bigint auto_increment not null,
  person_id                     bigint not null,
  game_id                       bigint not null,
  team_id                       bigint not null,
  minute                        integer not null,
  m_offset                      integer not null,
  type                          varchar(7) not null,
  own_goal                      boolean default false not null,
  version                       bigint not null,
  when_created                  timestamp not null,
  when_modified                 timestamp not null,
  constraint ck_goals_type check ( type in ('GAME','PENALTY')),
  constraint pk_goals primary key (id)
);

create table groupz (
  id                            bigint auto_increment not null,
  pos                           bigint not null,
  title                         varchar(150) not null,
  event_id                      bigint not null,
  version                       bigint not null,
  when_created                  timestamp not null,
  when_modified                 timestamp not null,
  constraint pk_groupz primary key (id)
);

create table groupz_teams (
  groupz_id                     bigint not null,
  teams_id                      bigint not null,
  constraint pk_groupz_teams primary key (groupz_id,teams_id)
);

create table leagues (
  id                            bigint auto_increment not null,
  title                         varchar(150) not null,
  type                          varchar(8) not null,
  association_id                bigint not null,
  version                       bigint not null,
  when_created                  timestamp not null,
  when_modified                 timestamp not null,
  constraint ck_leagues_type check ( type in ('CLUB','NATIONAL')),
  constraint pk_leagues primary key (id)
);

create table persons (
  id                            bigint auto_increment not null,
  name                          varchar(150) not null,
  country_id                    bigint not null,
  version                       bigint not null,
  when_created                  timestamp not null,
  when_modified                 timestamp not null,
  constraint pk_persons primary key (id)
);

create table rounds (
  id                            bigint auto_increment not null,
  pos                           bigint not null,
  title                         varchar(150) not null,
  type                          varchar(8) not null,
  event_id                      bigint not null,
  start_timestamp               datetime not null,
  end_timestamp                 datetime not null,
  constraint ck_rounds_type check ( type in ('GROUP','KNOCKOUT')),
  constraint pk_rounds primary key (id)
);

create table seasons (
  id                            bigint auto_increment not null,
  title                         varchar(150) not null,
  version                       bigint not null,
  when_created                  timestamp not null,
  when_modified                 timestamp not null,
  constraint pk_seasons primary key (id)
);

create table teams (
  id                            bigint auto_increment not null,
  title                         varchar(150) not null,
  type                          varchar(8) not null,
  association_id                bigint not null,
  version                       bigint not null,
  when_created                  timestamp not null,
  when_modified                 timestamp not null,
  constraint ck_teams_type check ( type in ('CLUB','NATIONAL')),
  constraint pk_teams primary key (id)
);

create index ix_associations_country_id on associations (country_id);
alter table associations add constraint fk_associations_country_id foreign key (country_id) references countries (id) on delete cascade on update cascade;

create index ix_associations_associations_associations_1 on associations_associations (parent_association_id);
alter table associations_associations add constraint fk_associations_associations_associations_1 foreign key (parent_association_id) references associations (id) on delete restrict on update restrict;

create index ix_associations_associations_associations_2 on associations_associations (child_association_id);
alter table associations_associations add constraint fk_associations_associations_associations_2 foreign key (child_association_id) references associations (id) on delete restrict on update restrict;

create index ix_countries_continent_id on countries (continent_id);
alter table countries add constraint fk_countries_continent_id foreign key (continent_id) references continents (id) on delete restrict on update restrict;

create index ix_events_league_id on events (league_id);
alter table events add constraint fk_events_league_id foreign key (league_id) references leagues (id) on delete restrict on update restrict;

create index ix_events_season_id on events (season_id);
alter table events add constraint fk_events_season_id foreign key (season_id) references seasons (id) on delete restrict on update restrict;

create index ix_events_teams_events on events_teams (events_id);
alter table events_teams add constraint fk_events_teams_events foreign key (events_id) references events (id) on delete restrict on update restrict;

create index ix_events_teams_teams on events_teams (teams_id);
alter table events_teams add constraint fk_events_teams_teams foreign key (teams_id) references teams (id) on delete restrict on update restrict;

create index ix_games_person_id on games (person_id);
alter table games add constraint fk_games_person_id foreign key (person_id) references persons (id) on delete restrict on update restrict;

create index ix_games_team_id on games (team_id);
alter table games add constraint fk_games_team_id foreign key (team_id) references teams (id) on delete restrict on update restrict;

create index ix_games_event_id on games (event_id);
alter table games add constraint fk_games_event_id foreign key (event_id) references events (id) on delete restrict on update restrict;

create index ix_goals_person_id on goals (person_id);
alter table goals add constraint fk_goals_person_id foreign key (person_id) references persons (id) on delete restrict on update restrict;

create index ix_goals_game_id on goals (game_id);
alter table goals add constraint fk_goals_game_id foreign key (game_id) references games (id) on delete restrict on update restrict;

create index ix_goals_team_id on goals (team_id);
alter table goals add constraint fk_goals_team_id foreign key (team_id) references teams (id) on delete restrict on update restrict;

create index ix_groupz_event_id on groupz (event_id);
alter table groupz add constraint fk_groupz_event_id foreign key (event_id) references events (id) on delete restrict on update restrict;

create index ix_groupz_teams_groupz on groupz_teams (groupz_id);
alter table groupz_teams add constraint fk_groupz_teams_groupz foreign key (groupz_id) references groupz (id) on delete restrict on update restrict;

create index ix_groupz_teams_teams on groupz_teams (teams_id);
alter table groupz_teams add constraint fk_groupz_teams_teams foreign key (teams_id) references teams (id) on delete restrict on update restrict;

create index ix_leagues_association_id on leagues (association_id);
alter table leagues add constraint fk_leagues_association_id foreign key (association_id) references associations (id) on delete restrict on update restrict;

create index ix_persons_country_id on persons (country_id);
alter table persons add constraint fk_persons_country_id foreign key (country_id) references countries (id) on delete cascade on update cascade;

create index ix_rounds_event_id on rounds (event_id);
alter table rounds add constraint fk_rounds_event_id foreign key (event_id) references events (id) on delete restrict on update restrict;

create index ix_teams_association_id on teams (association_id);
alter table teams add constraint fk_teams_association_id foreign key (association_id) references associations (id) on delete restrict on update restrict;


import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

group = "ru.kovpas.footbot"
version = "0.1.0"

buildscript {
    val kotlin_version: String by project

    repositories {
        maven { url = uri("http://dl.bintray.com/kotlin/kotlin-eap") }
        jcenter()
    }

    dependencies {
        classpath("org.jetbrains.kotlin:kotlin-gradle-plugin:$kotlin_version")
    }
}

allprojects {
    buildscript {
        repositories {
            maven { url = uri("https://plugins.gradle.org/m2/") }
            mavenCentral()
            jcenter()
        }
    }

    repositories {
        maven { url = uri("http://dl.bintray.com/kotlin/kotlin-eap") }
        maven { url = uri("https://kotlin.bintray.com/kotlinx") }
        mavenCentral()
        jcenter()
    }

    tasks.withType<Test> {
        useJUnitPlatform()
        testLogging {
            events("passed", "skipped", "failed")
        }
    }

    tasks.withType<KotlinCompile>().all {
        kotlinOptions {
            jvmTarget = "1.8"
        }
    }
}

tasks.create("run-server").dependsOn(":footbot-server:footbot-server-backend:run")
tasks.create("run-frontend").dependsOn(":footbot-server:footbot-server-frontend:run")


tasks.create("run-all") {
    dependsOn("run-server")
    dependsOn("run-frontend")
}

plugins {
    base
}

dependencies {
    subprojects.forEach {
        archives(it)
    }
}
